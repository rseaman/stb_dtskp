#!/usr/bin/perl -w

# GETSCHEDULE -- script to retrieve NOAO proposal information and build
# local keyword=value text database to allow iSTB to update each incoming
# header with proposal ID, proprietary period and other pertinent info.
#
# usage: getschedule -tel=<telname> -year=<year> -month=<month>>
#
# R. Seaman, 2004-07-09, based on services and code by D. Gasson

use strict ;

use Getopt::Long;
use SOAP::Lite;
use XML::XPath;
use XML::XPath::XMLParser;

# wrap the month length at either end, will explicitly handle leap days later
my @mlen = (31, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31, 31);
my @mname = ("December", "January", "February", "March", "April", "May",
    "June", "July", "August", "September", "October", "November", "December",
    "January");

my ($tel, $year, $month, $date, $sdate, $pnum, $inum, $day, $ndays, $isnoao);
my ($definst, $defcopy, $defprop, $prop, $prop1, $pid, $period, $pi, $aff);

GetOptions ('tel=s' => \$tel, 'year=s' => \$year, 'month=s' => \$month,
'definst=s' => \$definst, 'defcopy=s' => \$defcopy, 'defprop=s' => \$defprop);

# defaults for testing
$tel ||= 'kp4m';
$year ||= 2004;
$month ||= 8;
$definst ||= 'NOAO';
$defcopy ||= 'AURA';
$defprop ||= 'noao';

my $response = SOAP::Lite
    -> uri ('http://www.noao.edu/Andes/ScheduleSearch')
    -> proxy ('http://www.noao.edu/cgi-bin/webservices/andes/dispatch.pl');

my $propXml;

# this will fail on 29 February 2400
$ndays = $mlen[$month];
if ($month == 2 && $year%4 == 0 && $year%100 != 0) { $ndays++; }

print "# $tel observing schedule for $mname[$month] $year \n\n";

for ($day = 1; $day <= $ndays; $day++) {
    $isnoao = 0;

    $date = sprintf ("%4d-%02d-%02d", $year, $month, $day);
    $sdate = sprintf ("%4d%02d%02d", $year, $month, $day);
    # print $date, "\n"; next;

    my $result = $response->getProposalsScheduledOn ($tel, $date);
    unless ($result->fault) {
	$propXml = $result->result ();
    } else {
	print join ', ', $result->faultcode, $result->faultstring;
    }

    my $xp = XML::XPath->new (xml => $propXml);
    # my $timestamp = $xp->findvalue ('resultSet/query/timestamp');

    my @proposals = $xp->find ('resultSet/records/proposal')->get_nodelist ;
    if (! scalar @proposals) {
	print "institution[$sdate]  = $definst\n";
	print "proposal[$sdate]     = $defprop\n";
	print "investigator[$sdate] = unknown\n";
	print "affiliation[$sdate]  = unknown\n";
	print "title[$sdate]        = unknown\n";
	print "proprietary[$sdate]  = unknown\n";
	print "copyright[$sdate]    = $defcopy\n\n";
	next;
    }

    $pnum = 1;
    foreach $prop (@proposals) {
	if ($pnum == 1) {
	    $pid = $prop->findvalue ('@noao:id');
	    if ($pid gt "") { $isnoao = 1; }

	    $pid ||= $defprop;

	    $period = $prop->findvalue ('noao:runs/noao:configuration[1]/' .
		'parameter' . '[@noao:type="proprietaryPeriod"]');
	    $period ||= '18';

	    $prop1 = $prop;

	} else {
	    # $pid = "multiple";
	    $pid = $defprop;
	    $period = "unknown";
	    last;
	}

	$pnum++;
    }

    my $title = $prop1->find ('title');

    if ($isnoao == 1) {
	print "institution[$sdate]  = NOAO\n";
    } else {
	print "institution[$sdate]  = $definst\n";
    }

    print "proposal[$sdate]     = $pid\n";

    my $investigators = $prop1->find ('investigators/investigator');

    $inum = 1;
    foreach my $invest ($investigators->get_nodelist) {
	if ($inum == 1) {
	    print "investigator[$sdate] = ",
		$invest->findvalue ('name/first'), " ",
		$invest->findvalue ('name/last'), "\n";
	    print "affiliation[$sdate]  = ",
		$invest->findvalue ('affiliation'), "\n";
	    last;
	}

	$inum++;
    }

    print "title[$sdate]        = $title\n";
    print "proprietary[$sdate]  = $period\n";
    print "copyright[$sdate]    = $defcopy\n\n";
}
