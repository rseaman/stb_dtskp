OBJS1=header.o status.o queue.o datetime.o directory.o checksum.o sites.o schedule.o
OBJS2=header.o datetime.o checksum.o tape.o

LIBS=	-lm

all:	bitf lsfits sum32 checksites checkschedule dtsfilter dtsstart dtsstop dtslogger dtstracker stblogger dopropid

client:	lsfits sum32 dtslogger dtstracker dopropid

bitf:	bitf.o $(OBJS1)
	cc bitf.o $(OBJS1) $(LIBS) -o bitf

dopropid:       dopropid.o schedule.o datetime.o sites.o
	cc -Bstatic dopropid.o schedule.o sites.o datetime.o -o dopropid

sum32:	sum32.o checksum.o
	cc -Bstatic sum32.o checksum.o -o sum32

dtslogger:	dtslogger.o directory.o datetime.o
	cc dtslogger.o directory.o datetime.o -o dtslogger

dtstracker: dtstracker.o header.o directory.o checksum.o datetime.o
	cc -Bstatic dtstracker.o header.o checksum.o directory.o datetime.o -lm -o dtstracker

stblogger:	stblogger.o directory.o datetime.o
	cc stblogger.o directory.o datetime.o -o stblogger

catstat: catstat.o $(OBJS2)
	cc catstat.o $(OBJS2) -o catstat

lsfits:	lsfits.o header.o directory.o checksum.o datetime.o
	cc lsfits.o header.o checksum.o directory.o datetime.o -lm -o lsfits

lpcbits: lpcbits.o
	cc lpcbits.o -o lpcbits

checksites:	checksites.o sites.o
	cc checksites.o sites.o -o checksites

checkschedule:	checkschedule.o schedule.o datetime.o
	cc checkschedule.o schedule.o datetime.o -o checkschedule

dtsfilter:	dtsfilter.o status.o datetime.o queue.o
	cc dtsfilter.o status.o datetime.o queue.o -o dtsfilter

dtsstart:	dtsstart.o status.o datetime.o queue.o
	cc dtsstart.o status.o datetime.o queue.o -o dtsstart

dtsstop:	dtsstop.o status.o datetime.o queue.o
	cc dtsstop.o status.o datetime.o queue.o -o dtsstop

zzdir:	zzdir.o directory.o
	cc zzdir.o directory.o -o zzdir

zzdate:	zzdate.o header.o checksum.o datetime.o
	cc zzdate.o header.o checksum.o datetime.o -o zzdate

zztime:	zztime.o datetime.o
	cc zztime.o datetime.o -o zztime

twodaysago:	twodaysago.o datetime.o
	cc twodaysago.o datetime.o -o twodaysago

zzitime: zzitime.o datetime.o
	cc zzitime.o datetime.o -o zzitime

zzheader: zzheader.o header.o checksum.o datetime.o
	cc zzheader.o header.o checksum.o datetime.o -o zzheader


bitf.o:	bitf.c config.h header.h status.h directory.h sites.h schedule.h
	cc -c -O bitf.c

dopropid.o:       dopropid.c config.h schedule.h sites.h
	cc -c -O dopropid.c

sum32.o: sum32.c config.h
	cc -c -O sum32.c

dtstracker.o: dtstracker.c config.h header.h
	cc -c -O dtstracker.c

catstat.o: catstat.c config.h header.h telescopes.h
	cc -c -O catstat.c

lsfits.o: lsfits.c config.h header.h
	cc -c -O lsfits.c

lpcbits.o: lpcbits.c config.h
	cc -c -O lpcbits.c

checksum.o: checksum.c config.h
	cc -c -O checksum.c

sites.o: sites.c sites.h config.h
	cc -c -O sites.c

schedule.o: schedule.c schedule.h config.h
	cc -c -O schedule.c

dtslogger.o: dtslogger.c config.h
	cc -c -O dtslogger.c

stblogger.o: stblogger.c config.h
	cc -c -O stblogger.c

zzdir.o: zzdir.c
	cc -c -O zzdir.c

checksites.o: checksites.c config.h sites.h
	cc -c -O checksites.c

checkschedule.o: checkschedule.c config.h schedule.h
	cc -c -O checkschedule.c

dtsfilter.o: dtsfilter.c config.h
	cc -c -O dtsfilter.c

dtsstart.o: dtsstart.c config.h
	cc -c -O dtsstart.c

dtsstop.o: dtsstop.c config.h
	cc -c -O dtsstop.c

zzdate.o: zzdate.c config.h header.h
	cc -c -O zzdate.c

zztime.o: zztime.c
	cc -c -O zztime.c

twodaysago.o: twodaysago.c
	cc -c -O twodaysago.c

zzitime.o: zzitime.c
	cc -c -O zzitime.c

header.o: header.c config.h header.h
	cc -c -O header.c

status.o: status.c config.h status.h
	cc -c -O status.c

queue.o: queue.c config.h
	cc -c -O queue.c

datetime.o: datetime.c config.h
	cc -c -O datetime.c

directory.o: directory.c config.h directory.h
	cc -c -O directory.c

# install:
# 	mv -f bitf lsfits sum32 checksites checkschedule dopropid \
# 	    dtsfilter dtsstart dtsstop dtslogger dtstracker stblogger ../../bin

install:
	mv -f bitf lsfits sum32 checksites checkschedule dopropid \
 	    dtsfilter dtsstart dtsstop dtslogger dtstracker stblogger /bits/bin

install_client:
	mv -f lsfits sum32 dtslogger dtstracker ../../bin

clean:
	rm -f bitf sum32 dtstracker catstat lsfits dtslogger stblogger \
	    checksites checkschedule dtsfilter dtsstart dtsstop \
	    zzdir zzdate zztime twodaysago zzitime zzheader core* *.o

# This should be executed with root privileges whenever bitf is
# recompiled and reinstalled:
chown:
	chown cache.cache /bits/bin/dopropid
	chmod 4755 /bits/bin/dopropid
	chown cache.cache /bits/bin/dtslogger
	chmod 4755 /bits/bin/dtslogger
	chown cache.cache /bits/bin/dtstracker
	chmod 4755 /bits/bin/dtstracker
	chown cache.cache /bits/bin/stblogger
	chmod 4755 /bits/bin/stblogger
	chown root.bits /bits/bin/bitf
	chmod 4755 /bits/bin/bitf


# This should be executed with root privileges ONLY when installing the
# package for the first time - it will clobber the current archive status.
# Changes may be needed first to stb_status and minfree (save copies).
# The `bits' account must be created prior to this.
setup_server:
	mkdir /bits/spool /bits/bin /noaocache/discards
	chown cache /bits/spool
	chgrp cache /bits/spool
	chmod 0770 /bits/spool
	chown bits /bits/bin
	chgrp bits /bits/bin
	chmod 0755 /bits/bin
	chown cache /noaocache/discards
	chgrp cache /noaocache/discards
	chmod 0770 /noaocache/discards
	ln -s /noaocache/discards /bits
	cp ../lib/stb_status /bits/spool
	chown cache /bits/spool/stb_status
	chgrp cache /bits/spool/stb_status
	chmod 0644 /bits/spool/stb_status
	mknod /bits/spool/null c 1 3
	chown cache /bits/spool/null
	chgrp cache /bits/spool/null
	chmod 0644 /bits/spool/null
	ln -s /bits/istb/scripts/dtsstatus /bits/bin
	ln -s /bits/istb/scripts/dts_trigger /bits/bin
	ln -s /bits/istb/scripts/getschedule /bits/bin
	ln -s /bits/istb/scripts/kickstart /bits/bin
	ln -s /bits/istb/schedule /bits

setup_client:
	mkdir /bits/fits /bits/bin
	chown cache /bits/fits
	chgrp cache /bits/fits
	chmod 0770 /bits/fits
	chown bits /bits/bin
	chgrp bits /bits/bin
	chmod 0755 /bits/bin
	mknod /bits/fits/null c 1 3
	chown cache /bits/fits/null
	chgrp cache /bits/fits/null
	chmod 0644 /bits/fits/null
	ln -s /bits/istb/scripts/postproc /bits/bin
	ln -s /bits/istb/scripts/fitsf /bits/bin
