/* DOPROPID -- update the PROPID cache on a particular instrument host.
 * When called with no argument, report the cached PROPID.
 *
 * R. Seaman - 2013-08-28, revised 2014-01-31, 2014-02-14
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "config.h"
#include "schedule.h"
#include "sites.h"

#define	ERR		0
#define	OK		1

main (argc, argv)
int	argc;
char	*argv[];
{
	struct	daily_schedule nightinfo, sch_info;
	struct	site_status	sinfo;
        char    propid[SZ_PATHNAME], title[SZ_PATHNAME], tmp[SZ_PATHNAME];
	char	ids[SZ_PATHNAME];
	char	hostname[SZ_PATHNAME], telename[SZ_PATHNAME], instname[SZ_PATHNAME];
	char	datestr[SZ_PATHNAME], sitename[SZ_PATHNAME], root_dir[SZ_PATHNAME];
	char	dir1_key[SZ_PATHNAME], dir2_key[SZ_PATHNAME], dir3_key[SZ_PATHNAME];
        char    touch_cmd[SZ_PATHNAME];
	int	timezone;
        int     status=OK, rstatus=OK, foundit=0;
	char	*id;
	FILE	*fd;

	umask (DEF_UMASK);

	if (gethostname (hostname, SZ_PATHNAME) != 0) {
	    printf ("error getting hostname\n");
            exit (-1);
	}

        if (! read_sites (SITES, &sinfo)) {
	    printf ("error reading sites file\n");
            exit (-1);
        }

	if (get_site_info (sinfo, hostname, sitename, &timezone, root_dir,
	    dir1_key, dir2_key, dir3_key, telename, instname) < 0) {

	    printf ("unknown data acquisition host\n");
            exit (-1);
	}

	/* can only request values valid on the current observing date
	 */
	strncpy (datestr, iso_date_string (now() - 3600*(PIVOT)), SZ_PATHNAME);

	if (get_schedule (telename, datestr, &nightinfo) == ERR) {
	    printf ("error reading telescope schedule\n");
            exit (-1);
	}

	/* likely still embedded assumptions that PROPID_CACHE exists, is readable, etc.
	 */

	if ((fd = fopen (PROPID_CACHE, "r")) == NULL) {
            unlink (PROPID_CACHE);   /* funny permissions ? */

            if ((fd = fopen (PROPID_CACHE, "a")) == NULL) {
	        printf ("error opening\n");
		exit (-1);
            } else {
       		fprintf (fd, "off\n");
		if (fflush (fd) || fsync (fileno(fd)) || fclose (fd)) {
                    printf ("error closing\n");
		    exit (-1);
       		}
            }

	    if ((fd = fopen (PROPID_CACHE, "r")) == NULL) {
		printf ("INTERNAL ERROR: can't open\n");
		exit (-1);
	    }
	}

	if (fscanf (fd, "%s", propid) != 1) {
	    printf ("error reading proposal ID\n");
	    exit (-1);
	}

	if (fclose (fd)) {
	    printf ("error closing\n");
	    exit (-1);
	}

	strcpy (ids, nightinfo.alt_ids);
	id = strtok (ids, " ");

	while (id != NULL) {
	    if (strcmp (propid, id) == 0) {
		foundit = 1;
		break;
	    }
	    id = strtok (NULL, " ");
	}

	/* stale values from previous sessions
	 */
	if (! foundit && strcmp (propid, "off") != 0) {
	    printf ("\n   proposal `%s' is stale\n", propid);

            if (unlink (PROPID_CACHE)) {
	        printf ("error unlinking\n");
		exit (-1);
            } else if ((fd = fopen (PROPID_CACHE, "a")) == NULL) {
	        printf ("error opening\n");
		exit (-1);
            } else {
		strcpy (propid, "off");
       		fprintf (fd, "off\n");
		if (fflush (fd) || fsync (fileno(fd)) || fclose (fd)) {
                    printf ("error closing\n");
		    exit (-1);
       		}
            }
	}

	if (argc > 1) {
	    if (strcmp (argv[1], "off") == 0 || strcmp (argv[1], "OFF") == 0) {
		strncpy (propid, "off", SZ_PATHNAME);

	    } else {
		strncpy (tmp, argv[1], SZ_PATHNAME);
		uppercase (tmp);

		if (tmp[0] != '2')
		    sprintf (propid, "20%s", tmp);
		else
		    strncpy (propid, tmp, SZ_PATHNAME);
	    }

	    if (get_proposal (propid, &sch_info) == ERR) {
		printf ("\n   proposal `%s' not found, try one of:\n\n", argv[1]);
		printf ("      %s\n\n", nightinfo.alt_ids);
		printf ("   `setpropid' to list current setting\n");
		printf ("   `setpropid off' for special handling after the fact\n\n");
		exit (-1);
	    }

	    if (unlink (PROPID_CACHE)) {
		printf ("error (1) updating proposal ID\n");
		exit (-1);

	    } else if ((fd = fopen (PROPID_CACHE, "a")) == NULL) {
		printf ("error (2) updating proposal ID\n");
		exit (-1);

	    } else {
		fprintf (fd, "%s\n", propid);

		if (fflush (fd) || fsync (fileno(fd)) || fclose (fd)) {
		    printf ("error (3) updating proposal ID\n");
		    exit (-1);
		}
            }
	}

	/* get the now controlled value afresh
	 */
	if (get_proposal (propid, &sch_info) == ERR) {
	    printf ("INTERNAL ERROR: proposal `%s' not found\n", propid);
	    exit (-1);
	}

	printf ("\n   Proposal ID   %s", sch_info.proposal);
	printf ("    (choose %s or `off')\n\n", nightinfo.alt_ids);

	if (strcmp (propid, "off") != 0 && strcmp (propid, "OFF") != 0) {
	    printf ("   Scheduled by  %s\n", sch_info.institution);
	    printf ("   Investigator  %s\n", sch_info.investigator);
	    printf ("   PI affil.     %s\n", sch_info.affiliation);
	    printf ("   Prog. title   %s\n\n", sch_info.title);
	    /* printf ("Copyright     %s\n", sch_info.copyright);
	     * printf ("Proprietary   %d months\n", sch_info.proprietary);
	     */
	}

	/* command to send results to server goes in script
	 * rstatus = system (PROPID_CMD);
	 * printf ("%d\n", rstatus);
	 */

	exit (0);
}


uppercase (sPtr)
char *sPtr;
{
	while (*sPtr != '\0') {
	    *sPtr = toupper ((unsigned char) *sPtr);
	    ++sPtr;
	}
}
