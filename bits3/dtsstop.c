#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "config.h"
#include "status.h"

main (argc, argv)
int	argc;
char	*argv[];
{
	struct queue_status	stb;

        if (! disable_printing())
            printf ("error disabling processing\n");

        if (! read_status (STATUS, &stb))
	    printf ("error reading status file %s\n", STATUS);

        stb.status = STOPPED;

        if (! write_status (stb, STATUS))
	    printf ("error writing status file %s\n", STATUS);

	exit (0);
}
