#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "config.h"
#include "schedule.h"

/* FILE	*mp = stdout;
 */
FILE	*mp;

#define	ERR	0
#define	OK	1

int	messages = 0;
int	ngotten;

main (argc, argv)
int	argc;
char	*argv[];
{
	char telescope[SZ_PATHNAME], date[SZ_PATHNAME], date_pub[SZ_PATHNAME];
	int	year, month, day;
	struct	daily_schedule sinfo;

	if (argc < 2) {
	    print_usage();
	    exit (-1);
	}

	strncpy (telescope, argv[1], SZ_PATHNAME);
	strncpy (date, argv[2], SZ_PATHNAME);

	if (dtm_decode_short (date, &year, &month, &day) == ERR) {
	    print_usage();
	    exit (-1);
	}

	if (year < MINYEAR || year > MAXYEAR) {
	    printf ("year out of bounds\n");
	    exit (-1);
	}

	if (month < 1 || month > 12) {
	    printf ("month out of bounds\n");
	    exit (-1);
	}

	if (day < 1 || day > month_length (year, month)) {
	    printf ("day out of bounds\n");
	    exit (-1);
	}

	if (get_schedule (telescope, date, &sinfo) == ERR) {
	    printf ("error getting schedule\n");
	    exit (-1);
	}

	if (sinfo.proposal[0] == (char) NULL)
	    strcpy (sinfo.proposal, "noprop");

	if (sinfo.proprietary >= 0) {
	    strcpy (date_pub, date);
	    if (expiration_date (date_pub, sinfo.proprietary) == ERR) {
		printf ("error calculating expiration date\n");
		exit (-1);
	    }

	} else
	    strcpy (date_pub, "none");

	printf ("\n# %s observing schedule for %4d-%02d-%02d:\n",
	    telescope, year, month, day);
	printf ("DTS_OBS  = %s\n", sinfo.institution);
	printf ("DTS_PROP = %s\n", sinfo.proposal);
	printf ("DTS_PI   = %s\n", sinfo.investigator);
	printf ("DTS_AFFL = %s\n", sinfo.affiliation);
	printf ("DTS_TTL  = %s\n", sinfo.title);
	printf ("DTS_PUB  = %s\n", date_pub);
	printf ("DTS_OWNR = %s\n", sinfo.copyright);
	printf ("DTS_ALT  = %s\n\n", sinfo.alt_ids);

	exit (0);
}


/* print an informative message
 */
print_usage ()
{
	printf ("usage: checkschedule <telescope> <iso_date>\n");
}
