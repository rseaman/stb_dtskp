/* LSFITS -- list info about FITS files.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "config.h"
#include "header.h"
#include "directory.h"

#define	ERR		0
#define	OK		1

#define	SZ_NAME		32

/* FILE	*mp = stdout;
 */
FILE	*mp;

int	messages = 0;
int	ngotten;

main (argc, argv)
int	argc;
char	*argv[];
{
	struct	fitsheader header;
	struct  dir        *dp, *zopdir();
	char	fname[SZ_PATHNAME];
	int	nflag, nfile, verbose=0, recid=0, list_only=0;
	int	lmessages=0;
	FILE	*fp;

	if (argc > 1) {
	    for (nflag=1; nflag <= 1 && nflag < argc; nflag++) {
		if (argv[nflag][0] != '-' || strlen (argv[nflag]) != 2)
		    break;

		if (argv[nflag][1] == 'r') {
		    recid=1;
		} else if (argv[nflag][1] == 'v') {
		    verbose=1;
		    lmessages=2;
		} else if (argv[nflag][1] == 'l') {
		    verbose=1;
		    lmessages=3;
		} else if (argv[nflag][1] == 'h') {
		    list_only=1;
		} else {
		    printf ("unknown command line flag `%s'\n", argv[nflag]);
		    print_usage ();
		    exit (-1);
		}
	    }
	}

	nflag = verbose || recid || list_only;

	if (argc <= nflag+1) {
	    if ((dp = zopdir (".")) == NULL) {
		printf ("error opening current working directory\n");
		exit (-1);
	    }

	    while (zgfdir (dp, fname, SZ_PATHNAME) != EOF) {
		if ((fp = fopen (fname, "r")) == NULL) {
		    printf ("error opening file %s\n", fname);
		    exit (-1);
		}

		if (read_header (fp, &header)) {
		    /* skip the ./ prefix for each filename
		     */
		    if (recid) {
			printf ("%s   %s\n", &fname[2], header.recid);
		    } else if (list_only) {
			print_header (stdout, header.buf, header.ncards);
		    } else if (verbose) {
			printf ("\nFile %s:\n", &fname[2]);
			describe_header (stdout, header, lmessages);
		    } else {
			printf ("%s  ", &fname[2]);
			short_description (stdout, header);
		    }

		    free_header (&header);
		}

		fclose (fp);
	    }

	    zcldir (dp);

	} else {
	    for (nfile=nflag+1; nfile < argc; nfile++) {
		if ((fp = fopen (argv[nfile], "r")) == NULL) {
		    printf ("error opening file %s\n", argv[nfile]);
		    exit (-1);
		}

		if (read_header (fp, &header)) {
		    if (recid) {
			printf ("%s   %s\n", argv[nfile], header.recid);
		    } else if (list_only) {
			print_header (stdout, header.buf, header.ncards);
		    } else if (verbose) {
			printf ("\n%s:\n", argv[nfile]);
			describe_header (stdout, header, lmessages);
		    } else {
			printf ("%s  ", argv[nfile]);
			short_description (stdout, header);
		    }

		    free_header (&header);

		} else
		    printf ("error opening FITS file %s\n", argv[nfile]);

		fclose (fp);
	    }
	}

	exit (0);
}


/* print an informative message
 */
print_usage ()
{
	printf ("usage: lsfits [-r|-v|-l|-h] <fits_files>\n");
}
