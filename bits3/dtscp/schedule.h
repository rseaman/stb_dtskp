#ifndef	SZ_SCHEDULE
#define	SZ_SCHEDULE	321
#endif

struct	daily_schedule {
	char institution[SZ_SCHEDULE];
	char proposal[SZ_SCHEDULE];
	char investigator[SZ_SCHEDULE];
	char affiliation[SZ_SCHEDULE];
	char title[SZ_SCHEDULE];
	char copyright[SZ_SCHEDULE];
	int proprietary;
};
