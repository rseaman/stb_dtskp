#ifndef	SZ_SITES
#define	SZ_SITES	32
#endif

struct	host_status {
	char name[MAX_NALT][SZ_SITES];
	int site;
	int telescope;
	int alias;
	int instrument;
};

struct	site_status {
	int nsites;
	int ntelescopes;
	int ninstruments;
	int nhosts;
	int nalternates;

	char locale[SZ_SITES];

	int timezone[MAX_NSITES];
	char site[MAX_NSITES][SZ_SITES];
	char root_dir[MAX_NSITES][SZ_SITES];
	char dir1_key[MAX_NSITES][SZ_SITES];
	char dir2_key[MAX_NSITES][SZ_SITES];
	char dir3_key[MAX_NSITES][SZ_SITES];

	char telescope[MAX_NTELESCOPES][SZ_SITES];
	char instrument[MAX_NINSTRUMENTS][SZ_SITES];

	struct	host_status host[MAX_NHOSTS];
};
