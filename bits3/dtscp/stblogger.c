/* STBLOGGER -- append a record to the STB log file on a mountain cache.
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "config.h"
#include "directory.h"

#define	ERR		0
#define	OK		1

main (argc, argv)
int	argc;
char	*argv[];
{
	int	status=0, itime=0;

	struct	dir	*dp, *zopdir();
	char	logfile[SZ_PATHNAME];
	char	caldate[16], caldate2[16];
        char    fname[SZ_PATHNAME], host[SZ_PATHNAME];
        int     nfile, i, hlen;

	FILE	*fd;

	itime = now ();
	sprintf (caldate, "%s", iso_date_string (itime - 3600*(PIVOT)));
	if (! dtm_from_short (caldate, caldate2)) {
	    printf ("STBLOGGER: date error\n");
	    exit (-1);
	}

	if (argc <= 1) {
            fd = stdout;

            if ((dp = zopdir (".")) == NULL) {
                fprintf (fd, "STBLOGGER: error opening CWD\n");
                exit (-1);
            }

            while (zgfdir (dp, fname, SZ_PATHNAME) != EOF)
                if (! dolog (fname, caldate2, itime, fd))
                    status = -1;

            zcldir (dp);

	} else {
            if (gethostname (host, SZ_PATHNAME) != 0) {
                printf ("STBLOGGER: error getting hostname\n");
                exit (-1);
            }

	    hlen = strlen (host);

            for (i=0; i < hlen; i++)
                if (host[i] == '.') {
                    host[i] = (char) NULL;
                    hlen = i + 1;
                    break;
                }

            if (hlen > 0)
                sprintf (logfile, "%s/%s_%s.log", DTSLOG, host, caldate);
            else
                sprintf (logfile, "%s/NO_HOST_%s.log", DTSLOG, caldate);

            if ((fd = fopen (logfile, "a")) == NULL) {
                printf ("STBLOGGER: error opening %s\n", logfile);
                exit (-1);
            }

            for (nfile=1; nfile < argc; nfile++)
                if (! dolog (argv[nfile], caldate2, itime, fd))
                    status = -1;

            if (fflush (fd) || fsync (fileno(fd)) || fclose (fd)) {
                printf ("STBLOGGER: error closing %s\n", logfile);
                status = -1;
            }
	}

	exit (status);
}


int dolog (fname, caldate, itime, fpout)
char	*fname;
char	*caldate;
int	itime;
FILE	*fpout;
{
	struct	stat	sbuf;
	char	mcaldate[16], mcaldate2[16];
	int	mtime, size, uid, gid, nlink;
	unsigned mode;

	if (stat (fname, &sbuf) != 0) {
	    fprintf (fpout, "%8s %10d %s _NO_STAT_\n", caldate, itime, fname);
	    return (ERR);

	} else {
	    size = (int) sbuf.st_size;
	    mtime = (int) sbuf.st_mtime;
	    uid = (int) sbuf.st_uid;
	    gid = (int) sbuf.st_gid;
	    mode = (unsigned) sbuf.st_mode;
	    nlink = (int) sbuf.st_nlink;

	    sprintf (mcaldate, "%s", iso_date_string (mtime - 3600*(PIVOT)));
	    if (! dtm_from_short (mcaldate, mcaldate2)) {
		fprintf (fpout, "%8s %10d %s %9d _DATE_ERROR_\n",
		    caldate, itime, fname, size);

		return (ERR);
	    }

	    fprintf (fpout, "%8s %10d %s %9d\n",
		mcaldate2, mtime, fname, size);
	}

	return (OK);
}
