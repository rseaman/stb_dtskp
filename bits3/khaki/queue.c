/* queue.c -- routines to perform privileged host queue operations,
 * to fetch the queue status information, and to start the daemon
 */

#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/resource.h>

#include "config.h"

#define	ERR		0
#define	OK		1

/* int get_queue_status (queuing, printing, active)
 * int     *queuing;
 * int     *printing;
 * int	*active;
 */
int get_queue_status ()
{
	char	cmd[SZ_PATHNAME];
	unsigned errseen;

	errseen = system (GET_QUEUE_STATUS);
	errseen >>= 8;

/*	*queuing = 1;
 *	*printing = 1;
 *	*active = 1;
 */

	if (errseen)
	    return (ERR);
	else
	    return (OK);
}


int get_daemon_pid (pid, active)
int	*pid;
int	*active;
{
	FILE	*fp;

	*pid = -1;
	*active = 0;

        if ((fp = fopen (LOCKFILE, "r")) != NULL) {
	    if (fscanf (fp, "%d", pid) != 1) {
		*pid = -1;	/* just to make sure */
		fclose (fp);
		return (ERR);
	    }

	    errno = 0;
	    getpriority (PRIO_PROCESS, *pid);
	    *active = (errno != ESRCH);

	    fclose (fp);
	}

        return (OK);
}


int enable_queuing ()
{
	unsigned errseen;

	/* run the necessary setuid root command
	 * and extract the exit status
	 */
	errseen = system (ENABLE_QUEUING);
	errseen >>= 8;

	if (errseen)
	    return (ERR);
	else
	    return (OK);
}


int disable_queuing ()
{
	unsigned errseen;

	/* run the necessary setuid root command
	 * and extract the exit status
	 */
	errseen = system (DISABLE_QUEUING);
	errseen >>= 8;

	if (errseen)
	    return (ERR);
	else
	    return (OK);
}


int enable_printing ()
{
	unsigned errseen;

	/* run the necessary setuid root command
	 * and extract the exit status
	 */
	errseen = system (ENABLE_PRINTING);
	errseen >>= 8;

	if (errseen)
	    return (ERR);
	else
	    return (OK);
}


int disable_printing ()
{
	unsigned errseen;

	/* run the necessary setuid root command
	 * and extract the exit status
	 */
	errseen = system (DISABLE_PRINTING);
	errseen >>= 8;

	if (errseen)
	    return (ERR);
	else
	    return (OK);
}


/*
 * int kill_daemon ()
 * {
 * 	unsigned errseen;
 */

	/* run the necessary setuid root command
	 * and extract the exit status
	 */
/*
 *	errseen = system (KILL_DAEMON);
 *	errseen >>= 8;
 *
 *	if (errseen)
 *	    return (ERR);
 *	else
 *	    return (OK);
 *}
 */


/* Start a new spooler daemon.  Rather than talk directly to the master
 * daemon using the lpd protocol, just enter a new file in the queue.
 * The stb status file is as good as any and may provide useful
 * debugging information on occasion.
 */
int kick_start ()
{
	char	cmd[SZ_PATHNAME];
	unsigned errseen;

	sprintf (cmd, KICKSTART, STATUS);
	errseen = system (cmd);

	errseen >>= 8;

	if (errseen)
	    return (ERR);
	else
	    return (OK);
}
