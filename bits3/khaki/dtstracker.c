/* DTSTRACKER -- append a record to the tracker file on an instrument computer.
 * Errors are written to the same log, or are silently discarded.
 * When called with no argument, logs contents of cwd to stdout.
 *
 * Dtslogger reads the date stamp propagated via the front-end queue
 * from dtslogger creating a traceable datekeeping system.
 *
 * R. Seaman - 2007-02-02
 */

#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>

#include "config.h"
#include "header.h"
#include "directory.h"

#define	ERR		0
#define	OK		1

FILE	*mp;

int	messages = 0;
int	ngotten;

main (argc, argv)
int	argc;
char	*argv[];
{
	int	status = 0, file_mode = 0, farg = 1;

	struct  dir        *dp, *zopdir();
	char	fname[SZ_PATHNAME], host[SZ_PATHNAME], logfile[SZ_PATHNAME];
	char	arg_class[SZ_PATHNAME], datestamp[SZ_PATHNAME];

	char	caldate[16], caldate2[16];
	int	iarg, nfile, i, hlen;
	FILE	*fd;

        datestamp[0] = (char) NULL;

        for (iarg = 1; iarg < argc; iarg++) {
            if (argv[iarg][0] == '-') {
                if (argv[iarg][1] == 'C') {
                    strcpy (arg_class, &argv[iarg][2]);

		    /* Read the traceable calendar datestamp, if present,
		     * as provided by dtslogger.  The format is an ISO 8601
		     * date ("extended", i.e., with hyphens) with a prefix.
		     * The test relies on a successful conversion to "basic"
		     * ISO (no hyphens), otherwise the string remains NULLed.
		     */
		    if (strncmp (arg_class, DATE_PREFIX, 1) == 0) {
			dtm_to_short (&arg_class[1], datestamp);
		    }

                /* } else if (argv[iarg][1] == 'F') {
		 *   file_mode = 1;
		 */

                } else {
		    farg = iarg;
		    break;
		}

	    } else {
		farg = iarg;
		break;
	    }
        }

	if (argc <= 1) {
	    fd = stdout;

	    if ((dp = zopdir (".")) == NULL) {
		printf ("DTSTRACKER: error opening CWD\n");
		status =-1;
	    }

	    while (zgfdir (dp, fname, SZ_PATHNAME) != EOF)
		if (! dotrack (fname, fd))
		    status = -1;

	    zcldir (dp);

	} else {
	    if (datestamp[0] != (char) NULL) {
		strcpy (caldate, datestamp);

	    } else {
		sprintf (caldate, "%s", iso_date_string (now() - 3600*(PIVOT)));
		if (! dtm_from_short (caldate, caldate2)) {
		    /* printf ("DTSTRACKER: date error\n");
		     */
		    exit (-1);
		}
	    }

	    if (gethostname (host, SZ_PATHNAME) != 0) {
/*		printf ("DTSTRACKER: error getting hostname\n");
 */
		exit (-1);
	    }

	    hlen = strlen (host);

	    for (i=0; i < hlen; i++)
		if (host[i] == '.') {
		    host[i] = (char) NULL;
		    hlen = i+1;
		    break;
		}

	    if (hlen > 0)
		sprintf (logfile, "%s/%s_%s.track", DTSPATH, host, caldate);
	    else
		sprintf (logfile, "%s/NO_HOST_%s.track", DTSPATH, caldate);

	    umask (DEF_UMASK);

	    if ((fd = fopen (logfile, "a")) == NULL) {
/*		printf ("DTSTRACKER: error opening %s\n", logfile);
 */
		exit (-1);
	    }

	    for (nfile=farg; nfile < argc; nfile++)
		if (! dotrack (argv[nfile], fd))
		    status = -1;

	    if (fflush (fd) || fsync (fileno(fd)) || fclose (fd)) {
/*		printf ("DTSTRACKER: error closing %s\n", logfile);
 */
		status = -1;
	    }
	}

	exit (status);
}

int dotrack (fname, fpout)
char	*fname;
FILE	*fpout;
{
	unsigned int	datasum;

	struct	fitsheader hdr, ehdr;
	char	caldate[SZ_PATHNAME], caldate_short[SZ_PATHNAME];
	int	idx, size, nheader=0, pixelerror=0, tz=TZ_DEFAULT, pivot=PIVOT;
	FILE	*fpin;

	if ((fpin = fopen (fname, "r")) == NULL) {
	    fprintf (fpout, "DTSTRACKER: error opening file %s\n", fname);
	    return (ERR);
	}

	if (read_header (fpin, &hdr)) {
	    if (! getcaldate (hdr.date_obs, hdr.date, hdr.ut,
		tz, pivot, caldate_short)) {

		strcpy (caldate, "_NO_DATE_");

	    } else if (! dtm_from_short (caldate_short, caldate)) {
		strcpy (caldate, "_NO_DATE_");
	    }

	    if (hdr.recid[0] != (char) NULL)
		fprintf (fpout, "# FITS %s %8s %s\n", fname,caldate,hdr.recid);
	    else
		fprintf (fpout, "# FITS %s %8s _NO_RECID_\n",fname,caldate);

	    if (hdr.pixelsize > 0) {
		if (! skip_pixels (fpin, hdr.pixelsize, &datasum))
		    pixelerror = 1;

	    } else
		datasum = 0;

	    if (pixelerror)
		fprintf (fpout, "%4d DATA_ERROR", nheader);
	    else
		fprintf (fpout, "%4d %010u", nheader, datasum);

	    fprintf (fpout, " %d %d %d %d %d", hdr.pixelsize,
		hdr.bitpix, hdr.gcount, hdr.pcount, hdr.naxis[0]);

	    for (idx=1; idx <= hdr.naxis[0]; idx++)
		fprintf (fpout, " %d", hdr.naxis[idx]);

	    fprintf (fpout, "\n");

	    free (hdr.buf);

	    while (! at_eof (fpin)) {
		pixelerror = 0;

		if (read_header (fpin, &ehdr)) {
		    nheader++;

		    if (! skip_pixels (fpin, ehdr.pixelsize, &datasum))
			pixelerror = 1;

		    if (pixelerror)
			fprintf (fpout, "%4d DATA_ERROR", nheader);
		    else
			fprintf (fpout, "%4d %010u", nheader, datasum);

		    fprintf (fpout, " %d %d %d %d %d", ehdr.pixelsize,
			ehdr.bitpix, ehdr.gcount, ehdr.pcount, ehdr.naxis[0]);

		    for (idx=1; idx <= ehdr.naxis[0]; idx++)
			fprintf (fpout, " %d", ehdr.naxis[idx]);

		    fprintf (fpout, "\n");

		    free (ehdr.buf);
		}
	    }

	    fclose (fpin);

	} else {
	    fprintf (fpout, "# OTHER %s %8s\n", fname, iso_date_string (now()));
	    fclose (fpin);
	}

	return (OK);
}
