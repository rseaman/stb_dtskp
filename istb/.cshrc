umask 022
setenv CVSROOT :ext:arcsoft@chive.tuc.noao.edu:/d2/arcsoft/REPOSITORY
setenv CVS_RSH /usr/bin/ssh

setenv PATH ${PATH}:/usr/sbin:/bits/bin:/xbits/bin

set     thishost = `hostname`
set     thisacct = `whoami`
set     prompt  = "$thisacct@$thishost% "

set     history = 100
set     notify
set     filec

alias   pb      'setenv TERM xterm-color; stty rows 44'
alias   im      'setenv TERM xterm-color; stty rows 66'
alias   wuzzup  'dtsstatus; rsh ctiot7 bitmon stat; rsh ctiot5 bitmon stat'
