## FITS /md1/36inch/nite3/dark174.fits 2010-01-10 kp09m.20100110T214010
#   0 0000000000 0 8 1 0 0
#   1 4080751454 6076 16 1 0 2 2136 4096
#   2 2223275139 6076 16 1 0 2 2136 4096
#   3 0251014746 6076 16 1 0 2 2136 4096
#   4 0917510541 6076 16 1 0 2 2136 4096
#   5 0804072692 6076 16 1 0 2 2136 4096
#   6 3893384958 6076 16 1 0 2 2136 4096
#   7 1134797317 6076 16 1 0 2 2136 4096
#   8 2391486540 6076 16 1 0 2 2136 4096

procedure dtschecker (dtacquis)

string	dtacquis		{prompt="Acquisition host"}

string	dtcaldat = "20100110"	{prompt="Observing calendar date"}
string	dtpropid = "2009B-0504"	{prompt="Proposal ID"}

string	trackfile = ""		{prompt="Override track file"}
string	fzimages = ""		{prompt="Override archive image list"}
#bool	checktests = yes	{prompt="Check test exposures?"}

string	archive_root = "/noaocache/mtn"	{prompt="Root for archive"}

struct	*list1, *list2, *list3

begin
	string	acq, dttelesc, images, fname1, fname2, img, img1, img2, img3
	string	j1, j2, j3, tmpfile1, tmpfile2, obsid1, obsid2
	string	obsid_stat, cksum_stat, ext_stat, key_stat, trackfile0, keyerrs

	string	obsid0, dttelesc0, dtcaldat0, dtpropid0, dtacquis0, dtacqnam0
	string	sb_host, sb_dir1, sb_dir2, sb_dir3, tmpval
	string	recno, dtnsanam, sb_recno, sb_id, sb_name
	string	dtacquis0_short, sb_host_short, timestr
	struct	datestr

	bool	firstline, obsid_mismatch, cksum_mismatch, errseen
	int	ij, len, slash, quote, extcount, filcount
	int	starttime, endtime, runtime, nskip
	int	fitsrec, fzrec, bytesz
	real	fitssz, fzsz, compratio, r
	struct	line1, line2

	cache sections

	errseen = no
	nskip = 0
	fitssz = 0
	fzsz = 0

	tmpfile1 = mktemp ("tmp$dts_")
	tmpfile2 = mktemp ("tmp$dts_")
	img2 = ""

	acq = dtacquis

	if (acq=="khaki" || acq=="tan") {
	    dttelesc = "kp4m"
	} else if (acq=="lapis" || acq=="royal") {
	    dttelesc = "kp21m"
	} else if (acq=="indigo") {
	    dttelesc = "kpcf"
	} else if (acq=="taupe" || acq=="emerald") {
	    dttelesc = "kp09m"

	} else if (acq=="sand" || acq=="cork" || acq=="dust") {
	    dttelesc = "wiyn"

	} else if (acq=="ctioa1" || acq=="ctioa8" || acq=="ctioa9") {
	    dttelesc = "ct4m"
	} else if (acq=="ctioa2" || acq=="ctiojm") {
	    dttelesc = "ct15m"
	} else if (acq=="andicam") {
	    dttelesc = "ct13m"
	} else if (acq=="yale1m") {
	    dttelesc = "ct1m"
	} else if (acq=="ctioa4") {
	    dttelesc = "ct09m"

	} else if (acq=="soaric1" || acq=="soaric2" || acq=="soaric7") {
	    dttelesc = "soar"

	} else {
	    printf ("unknown dtacqui=%s\n", acq)
	    goto cleanup
	}

	if (strlen (fzimages) > 0) {
	    images = fzimages

	} else {
	    # would need to update this for DMS mode
	    printf ("%s/%s/%s/%s/*.fz\n", archive_root,dtcaldat,dttelesc,dtpropid) |
		scan (images)
	}

	sections (images, opt="fullname", > tmpfile2)
	if (sections.nimages <= 0) {
	    printf ("no images found: %s\n", images)
	    goto cleanup
	}

	if (strlen (trackfile) > 0) {
	    trackfile0 = trackfile

	} else {
	    printf ("/bits/track/%s/%s_%s.track\n", dtcaldat, acq, dtcaldat) |
		scan (trackfile0)
	}

	if (! access (trackfile0)) {
	    printf ("trackfile %s not found (rerun checktele?)\n", trackfile0)
	    goto cleanup
	}

	time | scan (datestr)
	printf ("\nSTART: %s\n\n", datestr)
	ij = fscan (datestr, j1, timestr)
	starttime = int (3600. * (real(timestr) + 0.5))

	filcount = 0

	list2 = trackfile0
	list3 = tmpfile2
	while (fscan (list3, img) != EOF) {
	    # avoid having to uncompress data from other instruments
	    # should simply build a better listhead
	    printf ("!/bits/bin/listhead %s | grep DTACQUIS\n", img) |
		cl | scan (line1)
	    ij = fscan (line1, j1, tmpval)

	    dtacquis0 = "BLANK"

	    len = strlen (tmpval)
	    if (len > 0) {
		quote = stridx ("'", tmpval)

		if (quote > 0)
		    dtacquis0 = substr (tmpval, quote+1, len)
		else
		    dtacquis0 = tmpval
	    }

	    len = strlen (acq); dtacquis0_short = substr (dtacquis0, 1, len)
	    if (dtacquis0 == "BLANK" || dtacquis0_short != acq) {
		printf ("    skip %s (%s)\n", dtacquis0_short, img)
		nskip += 1
		next
	    }

	    slash = strldx ("/", img)
	    len = strlen (img)
	    printf ("/tmp/%s\n", substr(img,slash+1,len)) | scan (img1)
	    len = strlen (img1)
	    img2 = substr (img1, 1, len-3)

	    # if (access(img2)) ...
#	    printf ("!/bits/bin/funpack -S %s > %s\n", img, img2) | cl
	    printf ("!/bits/bin/funpack -O %s %s\n", img2, img) | cl
#	    printf ("!/bits/bin/dtstracker -F %s\n",img2) | cl(>tmpfile1)
	    printf ("!/bits/bin/sqtracker -F %s\n",img2) | cl(>tmpfile1)

	    dir (img, long+) | scan (line1)
	    if (fscan (line1, j1, j2, bytesz) != 3) {
		printf ("error reading file size for %s\n", img)
		goto cleanup
	    } else if (bytesz <= 0) {
		printf ("file size error (%d) for %s\n", bytesz, img)
		goto cleanup
	    } else if ((bytesz % 2880) != 0) {
		printf ("size (%d) not multiple of 2880 for %s\n", bytesz, img)
		goto cleanup
	    }

	    fzrec = bytesz / 2880
	    fzsz += real (bytesz) / 1048576.

	    dir (img2, long+) | scan (line1)
	    if (fscan (line1, j1, j2, bytesz) != 3) {
		printf ("error reading file size for %s\n", img2)
		goto cleanup
	    } else if (bytesz <= 0) {
		printf ("file size error (%d) for %s\n", bytesz, img2)
		goto cleanup
	    } else if ((bytesz % 2880) != 0) {
		printf ("size (%d) not multiple of 2880 for %s\n", bytesz, img2)
		goto cleanup
	    }

	    fitsrec = bytesz / 2880
	    fitssz += real (bytesz) / 1048576.

	    compratio = real (fitsrec) / real (fzrec)

	    # check DT and SB self-consistancy in primary HDU here
	    # would need to distinguish DMS mode from raw mode
	    # perform more detailed header verification in additional tool

	    # obsid0, dttelesc0, dtcaldat0, dtpropid0, dtacquis0, dtacqnam0
	    # sb_host, sb_dir1, sb_dir2, sb_dir3
	    # recno, dtnsanam, sb_recno, sb_id, sb_name

	    keyerrs = ""

	    img3 = img2 // "[0]"

#	    dtacquis0 = "BLANK"; hselect(img3,"DTACQUIS",yes)|scan (dtacquis0)
#	    len = strlen (acq); dtacquis0_short = substr (dtacquis0, 1, len)
#	    if (dtacquis0 == "BLANK" || dtacquis0_short != acq)
#		keyerrs += " DTACQUIS"
#
#	    sb_host = "BLANK"; hselect(img3,"SB_HOST",yes)|scan (sb_host)
#	    len = strlen (acq); sb_host_short = substr (sb_host, 1, len)
#	    if (sb_host == "BLANK" || sb_host_short != acq)
#		keyerrs += " SB_HOST"
#
#	    if (strlen (keyerrs) > 0) {
#		delete (tmpfile1, verify-, >& "dev$null")
#		delete (img2, verify-, >& "dev$null")
#		next
#	    }

	    obsid0 = "BLANK"; hselect (img3, "OBSID", yes) | scan (obsid0)
	    if (obsid0 == "BLANK")
		hselect (img3, "RECID", yes) | scan (obsid0)
#	    if (obsid0 == "BLANK") keyerrs += " OBSID"

	    dttelesc0 = "BLANK"; hselect(img3,"DTTELESC",yes)|scan (dttelesc0)
	    if (dttelesc0 == "BLANK" || dttelesc0 != dttelesc) {
		keyerrs += " DTTELESC"
		errseen = yes
	    }

	    dtcaldat0 = "BLANK"; hselect(img3,"DTCALDAT",yes)|scan (dtcaldat0)
	    if (dtcaldat0 == "BLANK") {
		keyerrs += " DTCALDAT"
		errseen = yes
	    }

	    # needs a format conversion to remove hyphens
#	    if (dtcaldat0 == "BLANK" || dtcaldat0 != dtcaldat)
#		keyerrs += " DTCALDAT"

	    dtpropid0 = "BLANK"; hselect(img3,"DTPROPID",yes)|scan (dtpropid0)
	    if (dtpropid0 == "BLANK" || dtpropid0 != dtpropid) {
		keyerrs += " DTPROPID"
		errseen = yes
	    }

	    dtacqnam0 = "BLANK"; hselect(img3,"DTACQNAM",yes)|scan (dtacqnam0)
	    if (dtacqnam0 == "BLANK") {
		keyerrs += " DTACQNAM"
		errseen = yes
	    }

	    sb_dir1 = "BLANK"; hselect(img3,"SB_DIR1",yes)|scan (sb_dir1)
	    if (sb_dir1 == "BLANK" || sb_dir1 != dtcaldat) {
		keyerrs += " SB_DIR1"
		errseen = yes
	    }

	    sb_dir2 = "BLANK"; hselect(img3,"SB_DIR2",yes)|scan (sb_dir2)
	    if (sb_dir2 == "BLANK" || sb_dir2 != dttelesc) {
		keyerrs += " SB_DIR2"
		errseen = yes
	    }

	    sb_dir3 = "BLANK"; hselect(img3,"SB_DIR3",yes)|scan (sb_dir3)
	    if (sb_dir3 == "BLANK" || sb_dir3 != dtpropid) {
		keyerrs += " SB_DIR3"
		errseen = yes
	    }

	    # various other crosschecks could be done
	    recno = "BLANK"; hselect(img3,"RECNO",yes)|scan (recno)
	    if (recno == "BLANK") {
		keyerrs += " RECNO"
		errseen = yes
	    }

	    dtnsanam = "BLANK"; hselect(img3,"DTNSANAM",yes)|scan (dtnsanam)
	    if (dtnsanam == "BLANK") {
		keyerrs += " DTNSANAM"
		errseen = yes
	    }

	    sb_recno = "BLANK"; hselect(img3,"SB_RECNO",yes)|scan (sb_recno)
	    if (sb_recno == "BLANK" || sb_recno != recno) {
		keyerrs += " SB_RECNO"
		errseen = yes
	    }

	    sb_id = "BLANK"; hselect(img3,"SB_ID",yes)|scan (sb_id)
	    if (sb_id == "BLANK") {
		keyerrs += " SB_ID"
		errseen = yes
	    }

	    sb_name = "BLANK"; hselect(img3,"SB_NAME",yes)|scan (sb_name)
	    if (sb_name == "BLANK" || sb_name != dtnsanam) {
		keyerrs += " SB_NAME"
		errseen = yes
	    }

	    delete (img2, verify-, >& "dev$null")

	    filcount += 1

	    extcount = -1
	    obsid_mismatch = no
	    cksum_mismatch = no
	    firstline = yes
	    line2 = "BLANK"

            list1 = tmpfile1
	    while (fscan (list1, line1) != EOF) {
		# synchronize the two lists (instrument track log and cache)
		if (firstline) {
		    if (fscan (list2, line2) == EOF) {
			printf ("early end to trackfile (rerun checktele?)\n")
			errseen = yes
			goto cleanup
		    }

		    while (substr(line2, 1, 6) != "# FITS") {
			printf ("non-FITS: %s\n", line2)

			if (fscan (list2, line2) == EOF) {
			   printf("early end to trackfile (rerun checktele?)\n")
			    errseen = yes
			    goto cleanup
			}
		    }

		    obsid1 = "BLANK"
		    obsid2 = "BLANK"

		    if (fscan (line1, j1, j2, fname1, j3, obsid1) < 4) {
			printf ("bad archive tracking (%s): %s\n", img, line1)
			errseen = yes
			goto cleanup
		    }

		    if (fscan (line2, j1, j2, fname2, j3, obsid2) < 4) {
			printf ("bad instrument tracking (%s): %s\n",
			    trackfile0, line2)
			errseen = yes
			goto cleanup
		    }

		    if (obsid1 == "_NO_RECID_")
		        obsid1 = "BLANK"

		    if (obsid2 == "_NO_RECID_")
		        obsid2 = "BLANK"

#		    if (obsid1 == "BLANK")
#			printf ("OBSID missing from archive\n")
#
#		    if (obsid2 == "BLANK")
#			printf ("OBSID missing from trackfile\n")

		    # exposures missing OBSIDs are labeled "BLANK", below
		    if (obsid2 != obsid1) {
			# allow for test exposures
#			printf ("OBSID mismatch!\n")
			obsid_mismatch = yes
			errseen = yes
		    }

		    if (obsid0 != obsid2) {
			obsid_mismatch = yes
			errseen = yes
		    }

		    # should never occur
		    if (obsid0 != obsid1) {
			keyerrs += " OBSID_INTERNAL"
			errseen = yes
		    }

		    firstline = no
		    next
		}

		# remaining lines are checksums, one per FITS extension
		# checks datasum, npixrec, bitpix, gcount, pcount, naxisN
		#   1 4080751454 6076 16 1 0 2 2136 4096

		if (fscan (list2, line2) == EOF) {
		    printf ("early end to trackfile (rerun checktele?)\n")
		    errseen = yes
		    goto cleanup
		}

		extcount += 1

#		printf ("line1 = %s\n", line1)
#		printf ("line2 = %s\n", line2)

		if (line2 != line1) {
#		    printf ("CHECKSUM mismatch!\n")
		    cksum_mismatch = yes
		    errseen = yes
		}
	    }
	    list1 = ""

	    delete (tmpfile1, verify-, >& "dev$null")

	    # verify OBSID and all extensions
	    if (obsid_mismatch)
		obsid_stat = "MISMATCH"
	    else
		obsid_stat = obsid1

	    if (cksum_mismatch)
		cksum_stat = "FAIL"
	    else
		cksum_stat = "pass"

	    if (extcount < 0) {
		ext_stat = "[NO_DATA]"
		cksum_stat = "FAIL"
		errseen = yes
	    } else
		printf ("[1+%s]\n", extcount) | scan (ext_stat)

	    if (strlen (keyerrs) > 0)
		key_stat = "KW_ERR"
	    else
		key_stat = "kw_ok"
	
	    printf ("%3d %s %s (%s) -> %s %s R=%4.2f %s\n",
		filcount, cksum_stat, fname2, obsid_stat, img,
		ext_stat, compratio, key_stat)

	    if (strlen (keyerrs) > 0)
		printf ("    ERRORS:%s\n", keyerrs)
	}

	if (fscan (list2, line2) != EOF)
	    printf ("entries remain in trackfile (queue not drained?)\n")

cleanup:
	list1 = ""; list2 = ""; list3 = ""
	delete (img2, verify-, >& "dev$null")
	delete (tmpfile1, verify-, >& "dev$null")
	delete (tmpfile2, verify-, >& "dev$null")

	if (errseen)
	    printf ("\nERRORS encountered")
	else
	    printf ("\nverification passed")

	printf (" for %s (%s) on %s\n", acq, dtpropid, dtcaldat)

	printf ("%d files", filcount)
	if (nskip > 0) printf (" (skipped %d)", nskip)

	r = 99.0
	if (filcount > 0) {
	    r = fitssz / fzsz
	    printf (", R=%4.2f, %5.2f MB -> %4.2f MB\n", r, fitssz, fzsz)
	} else
	    printf ("\n")

	time | scan (datestr)
	ij = fscan (datestr, j1, timestr)
	endtime = int (3600. * (real(timestr) + 0.5))
	runtime = endtime - starttime

	if (runtime < 0) {
	    runtime += 86400
	    if (runtime < 0) printf ("error with runtime\n")
	}

	printf ("runtime=%4.0m (%4.2fs/file, %5.3fs/MB)\n",
	    real(runtime)/60., real(runtime)/filcount, real(runtime)/fzsz)

	printf ("\ncut & paste > ok    ok   %3d   %6.1f  %4.2f", filcount, fzsz, r)

	if (dtpropid == "smarts")
	    printf ("  queue (SMARTS) <\n")
	else
	    printf ("  %s <\n", dtpropid)

	printf ("\nEND %s\n", datestr)
end
