procedure matchtele (logfile, trackfile)

string	logfile
string	trackfile

struct	*list1, *list2

begin
	string	l_logfile, l_trackfile, fname1, fname2, obsid
	string	j1, j2, j3, j4, j5, tmpfile1, tmpfile2, tmpfile3
	int	ij

	tmpfile1 = mktemp ("tmp$dts_")
	tmpfile2 = mktemp ("tmp$dts_")
	tmpfile3 = mktemp ("tmp$dts_")

	l_logfile = logfile
	l_trackfile = trackfile

	printf ("!grep FITS %s\n", l_trackfile) | cl (> tmpfile3)

	list1 = l_logfile
	while (fscan (list1, j1, j2, j3, j4, j5, fname1) != EOF) {
	    printf ("%s\n", fname1, >> tmpfile1)
	}

	list1 = l_logfile
	list2 = tmpfile3
	while (fscan (list2, j1, j2, fname2, j3, obsid) != EOF) {
	    printf ("%s\n", fname2, >> tmpfile2)
	}

#	list1 = l_logfile
#	list2 = tmpfile3
#	while (fscan (list1, j1, j2, j3, j4, j5, fname1) != EOF) {
#	    ij = fscan (list2, j1, j2, fname2, j3, obsid)
#
#	    if (fname2 == fname1) {
#		printf ("%s matches (%s)\n", fname1, obsid)
#	    } else {
#		printf ("%s != %s\n", fname1, fname2)
#	    }
#
#	    printf ("%s\n", fname1, >> tmpfile1)
#	    printf ("%s\n", fname2, >> tmpfile2)
#
#	    printf ("%s -> %s (%s)\n", fname1, fname2, obsid)
#	}

	printf ("!diff -y %s %s\n", osfn(tmpfile1), osfn(tmpfile2)) | cl
#	printf ("!comm %s %s\n", osfn(tmpfile1), osfn(tmpfile2)) | cl

	delete (tmpfile1, verify-)
	delete (tmpfile2, verify-)
	delete (tmpfile3, verify-)
end
