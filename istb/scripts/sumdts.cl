procedure sumdts (statsfile)

string	statsfile

struct	*list

begin
	string	lstats
	int	count, totcount
	real	fz, totfz, r, totfits
	string	j1, j2, j3, j4, j5

	lstats = statsfile

	totcount = 0
	totfits = 0.0
	totfz = 0.0

	list = lstats
	while (fscan (list, j1, j2, j3, j4, j5, count, fz, r) != EOF) {
	    if (nscan() != 8) next

	    totcount += count
	    totfz += fz
	    totfits += (r * fz)
	}

	r = totfits / totfz

	printf ("cut & paste > %d %8.1f  %4.2f %8.1f MB <\n",
	    totcount, totfz, r, totfits)
end
