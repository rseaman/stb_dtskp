procedure sumdts (statsfile)

string	statsfile

struct	*list

begin
	string	lstats
	int	count, totcount
	real	fz, totfz, rfz, rgz, fitssz, totfits, saved
	real	totover, overhead, totgz, diffsz, totsaved
	string	j1, j2, j3, j4, j5

	lstats = statsfile

	totcount = 0
	totfits = 0.0
	totfz = 0.0
	totgz = 0.0
	totsaved = 0.0
	totover = 0.0

	count=0; fz=0.0; rfz=0.0; rgz=0.0; fitssz=0.0; saved=0.0

	list = lstats
	while (fscan (list, j1, j2, count, fz, rfz, fitssz, j3, rgz, saved, j4, j5, overhead) != EOF) {
	    if (nscan() != 12) next

	    totcount += count
	    totfz += fz
	    totfits += fitssz
	    totgz += (fitssz / rgz)
	    totover += overhead
	    totsaved += (saved)

	    count=0; fz=0.0; rfz=0.0; rgz=0.0; fitssz=0.0; saved=0.0; overhead=0.0
	}

	rfz = totfits / totfz
	rgz = totfits / totgz

#	saved = 100. - 100. * (totfz / totgz)
	saved = 100. * (totsaved / totgz)

	printf (" total         %d %8.1f  %4.2f %8.1f MB    %4.2f %7.1f MB (%d%%)  %5.0m\n",
	    totcount, totfz, rfz, totfits, rgz, totsaved, saved, totover)
end
