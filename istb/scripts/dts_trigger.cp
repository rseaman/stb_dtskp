#!/bin/sh
# script called by bitf after writing file to trigger DTS transaction
# R. Seaman, 2 June 2004
# modified to generate MD5 and to handle non-compressible files, 3 Aug 3004

MAIL="seaman@noao.edu"

if test $# -ge 1
then
    FNAME=$1

    # echo $FNAME | /usr/bin/Mail -s "DTS trigger for $FNAME" $MAIL

    echo >> /tmp/dts_trigger_events

    if test ! -e $FNAME
    then
        echo `date` file $FNAME NOT FOUND >> /tmp/dts_trigger_events
        exit -1
    fi

    echo `date` compressing $FNAME >> /tmp/dts_trigger_events
    /bin/gzip $FNAME >> /tmp/dts_trigger_events 2>&1
    echo `date` compression done >> /tmp/dts_trigger_events

    CFNAME=$FNAME.gz

    if test -e $CFNAME
    then
        DFNAME=$CFNAME
    else
        DFNAME=$FNAME
    fi

    /bits/bin/stblogger $DFNAME

    echo `date` generating md5 >> /tmp/dts_trigger_events
    MD5VAL=`md5sum -b $DFNAME | sed s/\ .*$//`
    FILSIZ=`ls -o $DFNAME | awk '{printf("%d\n",$4)}'`
    echo `date` md5 done >> /tmp/dts_trigger_events

    echo `date` queuing $DFNAME >> /tmp/dts_trigger_events
    /usr/local/dcidata/bin/DciArchT -a "$DFNAME $MD5VAL $FILSIZ"
    echo `date` queuing done >> /tmp/dts_trigger_events

#    echo fifteen second cadencing tempo... >> /tmp/dts_trigger_events
#    /bin/sleep 15
#    echo `date` cadencing done >> /tmp/dts_trigger_events
fi

exit 0
