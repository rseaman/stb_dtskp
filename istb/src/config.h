/* directories that must be writable by the daemon
 */
#define	SPOOL		"/bits/spool"
#define DISCARDS	"/bits/discards"

/* text files read/written by bitf at runtime
 */
#define SITES		"/bits/istb/lib/stb_sites"
#define	FITS_COMMENTS	"/bits/istb/lib/fits.comments"
#define STATUS		"/bits/spool/stb_status"
#define MESSAGES	"/bits/spool/stb_messages"
#define	SCHEDULE	"/bits/schedule"
#define	REMED_LOG	"Remediation.log"

/* Postprocessing command to trigger subsequent transaction in the
 * chain of DTS forwarding - the argument is the full pathname to the
 * newly written file in the data cache.  Should be lightweight,
 * completion guaranteed, and run in the foreground.  Currently is
 * also used to compress the newly created file - this will perhaps
 * be replaced by a C callable gzip library at some point.
 */
#define DTSCMD		"/bits/bin/dts_trigger %s"

#define CACHE_ROOT	"/noaocache/mtn"
#define REMED_ROOT	"/noaocache/remed"

#define DTSPATH         "/bits/track"
#define DTSLOG          "/noaosw/log"

/* these require that the daemon acct (e.g., "lp") be given permission
 * for these operations - with LPRng, edit /etc/lpd/lpd.perms
 */
#define	ENABLE_QUEUING   "/usr/sbin/lpc enable dts"
#define	DISABLE_QUEUING  "/usr/sbin/lpc disable dts"
#define	ENABLE_PRINTING  "/usr/sbin/lpc start dts"
#define	DISABLE_PRINTING "/usr/sbin/lpc stop dts"
#define	GET_QUEUE_STATUS "/usr/sbin/lpc status dts | grep dts"
#define	KICKSTART	 "/usr/bin/lpr -Pdts %s > /dev/null 2>&1"

/* change through to here for alternate root */

#define	BLOCK		10		/* logical records/physical record */
#define	MEGABYTE	1048576		/* for human readable reports */

#define	MINYEAR		1900
#define	MAXYEAR		2399		/* leap heuristic fails in 2400 */
#define	Y2KPIVOT	1970		/* nonconforming old DATE-OBS format */

#define TZ_DEFAULT      -7	/* hours difference from UTC */
#define PIVOT   	12	/* local time when calendar date changes */
#define DATE_PREFIX	"D"	/* prefix for traceable dtslogger dates */
#define EPOCH_PREFIX	"U"	/* prefix for traceable dtslogger epochs */

/* access authorization control
 */
#define	DEF_UMASK		022	/* umask for status, etc. */
#define	DEF_PERMISSIONS		0755	/* subdirectory permissions */
#define DEF_USER		"cache"	/* default ownership */
#define DEF_GROUP		"cache"	/* default group ownership */
#define	DEF_UMASK_AUTH		027	/* umask for propid propid/files */
#define DEF_GROUP_AUTH		"noao"	/* default for propid/files */

/* maximum number of different site, telescope and instrument options
 */
#define	MAX_NSITES		8
#define	MAX_NTELESCOPES		32
#define	MAX_NINSTRUMENTS	32
#define	MAX_NHOSTS		128
#define	MAX_NALT		4

/* these line separates headers in the catalog
 * #define	SEPARATOR	"###\n"
 */
#define	SEPARATOR	"   \n"
#define	XSEPARATOR	"   \n"

/* read by catstat
 */
#define	TELESCOPES	"/bits/istb/lib/stb_telescopes"
#define	MAX_NALTERNATES	`	4     /* must be >= 2, even if no alternates */
#define	MAX_NDOMES		16    /* assumed >= NTELESCOPES, too */

/* the lock file is managed by lpd and owned by root
 */
#define	LOCKFILE	"/bits/spool/lock"

/* Command to create subdirectories of observer and recid image links.
 */
#define	LINKCMD		"/bits/bin/stblink %s > /dev/null 2>&1"

#ifndef	SZ_PATHNAME
#define	SZ_PATHNAME	161
#endif
