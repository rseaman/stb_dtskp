#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

/* FILE	*mp = stdout;
 */
FILE	*mp;

#define	ERR	0
#define	OK	1

int	messages = 0;
int	ngotten;

main (argc, argv)
int	argc;
char	*argv[];
{
	int	itime = 0;
	int	jtime = 0;
	int	mtime = 0;

	itime = now();
	jtime = now_ms(&mtime);

	printf ("%d seconds since 0h 1/1/1970\n", itime);
	printf ("%s\n", iso_date_string (itime));
	printf ("%s\n", utc_date_time (itime));

	printf ("%d.%03d seconds since 0h 1/1/1970\n", jtime, mtime);
	printf ("%s\n", iso_date_string (jtime));
	printf ("%s\n", utc_date_time_ms (jtime, mtime));

	exit (0);
}
