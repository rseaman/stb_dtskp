#define	RECORD		2880
#define	NCARD		36
#define	CARD		80

/* codes for fitsheader.type
 */
#define	SIMPLE		0
#define	GROUPS		1
#define	XTENSION	2
#define	UNKNOWN		3

#ifndef	SZ_PATHNAME
#define	SZ_PATHNAME	161
#endif

/* Really should handle sb_ and dts_ keywords dynamically in some fashion.
 * This is too verbose and application specific.  Maybe in a future update.
 */

struct	fitsheader {
	char	index[SZ_PATHNAME];
	char	filename[SZ_PATHNAME];
	int	type;

	char	extension[9];
	int	bitpix;
	int	naxis[1000];
	int	pcount;
	int	gcount;
	double	exptime;

	char	irafname[71];
	char	date_obs[71];
	char	odateobs[71];
	char	date[71];
	char	ut[71];

	char	telescope[71];
	char	imagetype[71];

	char	dts_site[71];
	char	dts_tele[71];
	char	dts_date[71];
	char	dts_utc[71];
	char	dts_inst[71];
	char	dts_prop[71];

	char	dts_name[71];
	char	dts_orig[71];
	char	dts_ownr[71];
	char	dts_host[71];
	char	dts_acct[71];

	char	dts_obs[71];
	char	dts_pi[71];
	char	dts_affl[71];
	char	dts_ttl[71];
	char	dts_pub[71];

	char	dts_stat[71];

	char	sb_host[71];
	char	sb_accou[71];
	char	sb_site[71];
	char	sb_local[71];

	char	sb_dir1[71];
	char	sb_dir2[71];
	char	sb_dir3[71];

	int	sb_recno;
	char	sb_id[71];
	char	sb_name[71];

	int	rmcount;
	char	rmcaldat[71];
	char	rmpropid[71];

	char	recid[71];
	int	recno;

	unsigned int datasum;
	unsigned int checksum;

	int	extendseen;
	int	groupsseen;
	int	pcountseen;
	int	gcountseen;

	int	irafnameseen;
	int	date_obsseen;
	int	odateobsseen;
	int	dateseen;
	int	utseen;

	int	recnoseen;
	int	checksumseen;
	int	datasumseen;

	int	dts_siteseen;
	int	dts_teleseen;
	int	dts_dateseen;
	int	dts_utcseen;
	int	dts_instseen;
	int	dts_propseen;

	int	dts_nameseen;
	int	dts_origseen;
	int	dts_ownrseen;
	int	dts_hostseen;
	int	dts_acctseen;

	int	dts_obsseen;
	int	dts_piseen;
	int	dts_afflseen;
	int	dts_ttlseen;
	int	dts_pubseen;

	int	dts_statseen;

	int	sb_hostseen;
	int	sb_accouseen;
	int	sb_siteseen;
	int	sb_localseen;

	int	sb_dir1seen;
	int	sb_dir2seen;
	int	sb_dir3seen;

	int	sb_recnoseen;
	int	sb_idseen;
	int	sb_nameseen;

	int	rmcountseen;
	int	rmcaldatseen;
	int	rmpropidseen;

	int	ncards;
	int	headersize;
	int	pixelsize;
	int	pixelpad;

	char	*buf;

	struct	fitsheader *next;
	struct	fitsheader *previous;
};
