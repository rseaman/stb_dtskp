/* CATSTAT -- Extract per telescope statistics from Save the Bits
 * format catalog files.
 *
 * Usage:
 *   catstat [-g] [-d|-m] [-s <offset>] [-i <#1> <#2>] <catalog_files>
 *
 * Command line flag:
 *  -g			only report the grand total
 *  -d			generate a header appropriate for a daily report
 *  -m			generate a header appropriate for a monthly report
 *  -c <caldate>	label line-by-line by the calendar date
 *  -s <offset>		seek to <offset> before reading the 1st file
 *  -i <#1> <#2>	only include statistics between these recnos (inc.)
 *
 * Argument:
 *      <catalog_files>
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "config.h"
#include "header.h"
#include "telescopes.h"

#define	ERR		0
#define	OK		1

#define	MAX_NIGHTS	256

#define	OBJECT1		"object"
#define	OBJECT2		"OBJECT"

/* statistics are accumulated in units of FITS records
 */
struct	statistics {
	char	name[SZ_PATHNAME];
	int	ntotal;
	int	total;
	int	nobject;
	int	object;
	int	ncalib;
	int	calib;
	int	minimum;
	int	maximum;
	double	exptime;
	int	initialized;
};

struct	night_statistics {
	struct	statistics grand;
	struct	statistics dome[MAX_NDOMES];
	int	initialized;
};

struct	catalog_statistics {
	struct	statistics grand;
	struct	statistics dome[MAX_NDOMES];
	struct	night_statistics night[MAX_NIGHTS];
	int	initialized;
};

FILE	*mp = stdout;
int	messages = 0;
int	ngotten;

main (argc, argv)
int	argc;
char	*argv[];
{
	char	caldate[SZ_PATHNAME];
	struct	obs_status obs;
	struct	catalog_statistics stats;
	struct	fitsheader header;
	int	nflag, ntele, nkey, size, nfile, nimage, offset, isobject;
	int	grand_total=0, daily=0, monthly=0, seek_first=0, first_time=1;
	int	recno1, recno2, recno_range=0, caldate_used=0, found_tele;
	double	exptime=0;
	FILE	*fp;

	if (argc <= 1) {
	    print_usage ();
	    exit (-1);

	} else {
	    for (nflag=1; nflag < argc; nflag++) {
		if (argv[nflag][0] != '-' || strlen (argv[nflag]) != 2)
		    break;

		if (argv[nflag][1] == 'g') {
		    grand_total=1;
		    continue;

		} else if (argv[nflag][1] == 'd') {
		    daily=1;
		    continue;

		} else if (argv[nflag][1] == 'm') {
		    monthly=1;
		    continue;

		} else if (argv[nflag][1] == 'c') {
		    if (++nflag >= argc) {
			print_usage ();
			exit (-1);
		    } else {
			strncpy (caldate, argv[nflag], SZ_PATHNAME);
		        caldate_used=1;
			continue;
		    }

		} else if (argv[nflag][1] == 's') {
		    if (++nflag >= argc) {
			print_usage ();
			exit (-1);
		    } else {
			if ((offset = atoi (argv[nflag])) < 0) {
			    printf ("bad seek specified, -s %s\n", argv[nflag]);
			    exit (-1);
			}
		        seek_first=1;
			continue;
		    }

		} else if (argv[nflag][1] == 'i') {
		    if (++nflag >= argc) {
			print_usage ();
			exit (-1);
		    } else {
			if ((recno1 = atoi (argv[nflag])) < 0) {
			    printf ("bad recno1 (%s) specified\n", argv[nflag]);
			    exit (-1);
			}

			if (++nflag >= argc) {
			    print_usage ();
			    exit (-1);
			} else {
			    if ((recno2 = atoi (argv[nflag])) < 0) {
				printf ("bad recno2 (%s) specified\n",
				    argv[nflag]);
				exit (-1);
			    }

			    recno_range=1;
			    continue;
			}
		    }

		} else {
		    printf ("unknown command line flag `%s'\n", argv[nflag]);
		    print_usage ();
		    exit (-1);
		}

	    }
	}

	nflag = grand_total + daily + monthly +
		caldate_used*2 + seek_first*2 + recno_range*3;

	if (argc - nflag > MAX_NIGHTS) {
	    printf ("too many catalog files specified");
	    exit (-1);
	}

	if (! read_telescopes (TELESCOPES, &obs)) {
	    printf ("error reading %s\n", TELESCOPES);
	    exit (-1);
	}

	/* initialize the dome names (and grand total label)
	 */
	strcpy (stats.grand.name, "Total");
	for (ntele=0; ntele < obs.ntelescopes; ntele++)
	    strcpy (stats.dome[ntele].name, obs.telescope[ntele].name);

	if (! caldate_used) { 
	    if (daily) {
		printf ("\nSave the Bits statistics for the night ending %s:\n",
		    time_string(now()));
		fflush (stdout);
	    } else if (monthly) {
		printf ("\nSave the Bits statistics for the month ending %s:\n",
		    time_string(now()));
		fflush (stdout);
	    }

	    if (! grand_total)
		print_labels ();
	}

	for (nfile=nflag+1; nfile < argc; nfile++) {
	    if ((fp = fopen (argv[nfile], "r")) == NULL) {
		printf ("error opening file %s\n", argv[nfile]);
		exit (-1);
	    }

	    if (first_time) {
		if (seek_first) {
		    if (fseek (fp, offset, 0) != 0) {
			printf ("error seeking to byte %d on file %s\n",
			    offset, argv[nfile]);
			exit (-1);
		    }
		}
		first_time=0;
	    }

	    /* initialize night name and night dome names
	     */
	    strcpy (stats.night[nfile].grand.name, argv[nfile]);
	    for (ntele=0; ntele < obs.ntelescopes; ntele++)
		strcpy (stats.night[nfile].dome[ntele].name,
		    obs.telescope[ntele].name);

	    nimage=0;
	    while (! at_eof (fp)) {
		nimage++;
		if (! skip_separator (fp)) {
		    printf ("error skipping separator\n");
		    break;
		}

		if (! read_catalog (fp, &header, 1)) {
		    printf ("error opening header %d\n", nimage);
		    break;
		}

		/* exclude images outside the specified range of recno's
		 * (inclusive at the bottom) - the images are presumed to
		 * be in increasing order by recno
		 */
		if (recno_range) {
		    if (header.recno < recno1)
			continue;
		    if (header.recno >= recno2)
			break;
		}

		size = (header.headersize + header.pixelsize);
		exptime = header.exptime;

		/* images missing the IMAGETYP keyword (or those with
		 * a blank value for IMAGETYP) are counted as objects
		 */
		isobject = ((! *header.imagetype) ||
		    (! strcmp (header.imagetype, OBJECT1)) ||
		    (! strcmp (header.imagetype, OBJECT2)));

		/* grand totals
		 */
		increment_stats (&stats.grand, size, exptime, isobject);
		increment_stats (&stats.night[nfile].grand, size, exptime,
		    isobject);

		/* totals by telescope
		 */
		found_tele = 0;
		for (ntele=0; ntele < obs.ntelescopes; ntele++) {
		    for (nkey=0; nkey < obs.nalternates; nkey++) {
/*
 * printf ("header = >%s<  <==>  telescope = >%s<\n",
 *    header.telescope, obs.telescope[ntele].key[nkey]);
 */
			if (! strcmp (header.telescope,
			    obs.telescope[ntele].key[nkey])) {

			    found_tele = 1;
			    break;
			}
		    }

		    if (found_tele)
			break;
		}

		/* last bin (e.g., "weather" or "other") by default
		 */
		if (! found_tele)
		    ntele = obs.ntelescopes - 1;

		increment_stats (&stats.dome[ntele], size, exptime, isobject);
		increment_stats (&stats.night[nfile].dome[ntele], size,
		    exptime, isobject);
	    }

	    free_header (&header);
	    fclose (fp);

	    if (!caldate_used && ! grand_total) {
		for (ntele=0; ntele < obs.ntelescopes; ntele++)
		    print_stats (stats.night[nfile].dome[ntele]);
		printf ("\n");
		print_stats (stats.night[nfile].grand);
		printf ("\n");
		printf ("\n");
	    }
	}

	nfile = --nfile - nflag;

	if (caldate_used) {
	    for (ntele=0; ntele < obs.ntelescopes; ntele++)
		print_stats_by_line (caldate, stats.dome[ntele]);

	} else if (nfile > 1 || grand_total) {
	    print_labels ();
	    for (ntele=0; ntele < obs.ntelescopes; ntele++)
		print_stats (stats.dome[ntele]);
	    printf ("\n");
	    print_stats (stats.grand);
	    printf ("\n");
	}

	exit (0);
}


/* the FITS headers are written to the archive catalog files separated
 * by some ascii string (e.g., an extra newline) - this also preceeds
 * the first header in each file
 */
int skip_separator (fp)
FILE *fp;
{
	char	buf[SZ_PATHNAME];

	if (fgets (buf, strlen(SEPARATOR)+1, fp) == NULL)
	    return (ERR);

	return (! strcmp (buf, SEPARATOR));
}


/* add the current image to the particular statistics
 */
increment_stats (stat, size, exptime, isobject)
struct	statistics *stat;
int	size;
double	exptime;
int	isobject;
{
	stat->total += size;
	stat->ntotal++;

	if (stat->initialized) {
	    if (size < stat->minimum)
		stat->minimum = size;
	    else if (size > stat->maximum)
		stat->maximum = size;
	} else {
	    stat->minimum = size;
	    stat->maximum = size;
	    stat->initialized = 1;
	}

	if (isobject) {
	    stat->nobject++;
	    stat->object += size;
	    if (exptime > 0)
		stat->exptime += exptime;
	} else {
	    stat->ncalib++;
	    stat->calib += size;
	}
}


/* print a line of column labels (and one of units)
 */
print_labels ()
{
	printf ("\n");
	printf ("   Telescope      Total          Object         Calib ");
	printf ("       Min   Max  Exposure\n");
	printf ("                  (#/Mb)         (#/Mb)         (#/Mb)");
	printf ("       (Mb)  (Mb)  (hours)\n");
	printf ("\n");
	fflush (stdout);
}


/* print a line summarizing the particular statistics (in Megabytes)
 * the exposure time handling doesn't need to be bulletproof, just reliable
 */
print_stats (stat)
struct	statistics stat;
{
	float	total, object, calib, minimum, maximum, exptime;
	int	hours, minutes;

	total	= (float)stat.total * RECORD / MEGABYTE;
	object	= (float)stat.object * RECORD / MEGABYTE;
	calib	= (float)stat.calib * RECORD / MEGABYTE;
	minimum	= (float)stat.minimum * RECORD / MEGABYTE;
	maximum	= (float)stat.maximum * RECORD / MEGABYTE;
	exptime	= (float)stat.exptime;

	if (exptime <= 0.0) {
	    exptime = 0.0;
	    hours = 0;
	    minutes = 0;

	} else {
	    hours = exptime / 3600;
	    minutes = (exptime/60.) - ((float)hours*60.) + 0.5;

	    while (minutes > 59) {
		minutes -= 60;
		hours += 1;
	    }

	    if (hours < 0)
		hours = 0;

	    if (minutes < 0)
		minutes = 0;
	}

	printf ("%12s  %6d/%-7.0f %6d/%-7.0f %6d/%-7.0f %6.2f%6.2f %4d:%02d\n",
	    stat.name, stat.ntotal, total, stat.nobject, object,
	    stat.ncalib, calib, minimum, maximum, hours, minutes);
	fflush (stdout);
}


/* print a summary line by date and telescope
 */
print_stats_by_line (caldate, stat)
char	*caldate;
struct	statistics stat;
{
	float	total, object, calib, minimum, maximum, exptime;
	int	hours, minutes;

	total	= (float)stat.total * RECORD / MEGABYTE;
	object	= (float)stat.object * RECORD / MEGABYTE;
	calib	= (float)stat.calib * RECORD / MEGABYTE;
	minimum	= (float)stat.minimum * RECORD / MEGABYTE;
	maximum	= (float)stat.maximum * RECORD / MEGABYTE;
	exptime	= (float)stat.exptime;

	if (exptime <= 0.0) {
	    exptime = 0.0;
	    hours = 0;
	    minutes = 0;

	} else {
	    hours = exptime / 3600;
	    minutes = (exptime/60.) - ((float)hours*60.) + 0.5;

	    while (minutes > 59) {
		minutes -= 60;
		hours += 1;
	    }

	    if (hours < 0)
		hours = 0;

	    if (minutes < 0)
		minutes = 0;
	}

	printf ("%12s  %-12s  %6d  %6d  %4d:%02d\n",
	    caldate, stat.name, stat.ntotal, stat.nobject, hours, minutes);
	fflush (stdout);
}


/* print an imformative message
 */
print_usage ()
{
printf ("usage: catstat [-g] [-d|-m] [-s <offset>] [-i <#1> <#2>] <catalog_files>\n");
}


/* read the configuration information for the list of telescopes
 */
int read_telescopes (telescopesfile, obs)
char *telescopesfile;
struct obs_status *obs;
{
	char	key[SZ_PATHNAME], value[SZ_PATHNAME], var[SZ_PATHNAME];
	int	n, na, ivalue, firsttime=1, gotntelescopes=0, gotnalternates=0;
	FILE	*fp;

	if ((fp = fopen (telescopesfile, "r")) == NULL)
	    return (ERR);

	for (n=0; n < MAX_NDOMES; n++)
	    for (na=0; na < MAX_NALTERNATES; na++)
		strcpy (obs->telescope[n].key[na], "NONE");

	/* read the telescopes file line by line, the number of
	 * telescopes and keyword alternates must precede the others
	 */
	while (! feof (fp)) {

/*
 * Need to scan multiple word telescope names:
 *
 *	    if (fscanf (fp, " %s = %s ", key, value) != 2)
 *		continue;
 */

	    if (fscanf (fp, " %s = %[^\n] ", key, value) != 2)
		continue;

	    if (isdigit (value[0]))
		ivalue = atoi (value);
	    else
		ivalue = -1;

	    if (strcmp (key, "ntelescopes") == 0) {
		if (! firsttime || ivalue <= 0 || ivalue >= MAX_NDOMES)
		    return (ERR);
		obs->ntelescopes = ivalue;
		gotntelescopes = 1;

            } else if (strcmp (key, "nalternates") == 0) {
		if (ivalue <= 0 || ivalue > MAX_NALTERNATES)
		    return (ERR);
                obs->nalternates = ivalue;
		gotnalternates = 1;

	    } else {
		if (! gotntelescopes || ! gotnalternates)
		    return (ERR);

                for (n=0; n < obs->ntelescopes; n++) {
		    sprintf (var, "telescope[%d].name", n);
		    if (strcmp (key, var) == 0) {
			strncpy (obs->telescope[n].name, value, SZ_PATHNAME);
			break;
		    }

		    for (na=0; na < obs->nalternates; na++) {
			sprintf (var, "telescope[%d].key[%d]", n, na);
			if (strcmp (key, var) == 0) {
			    strncpy (obs->telescope[n].key[na], value,
				SZ_PATHNAME);
			    break;
			}
		    }
                }
	    }

	    firsttime = 0;
	}

	fclose (fp);

	/* link the list
	 */
	for (n=0; n < obs->ntelescopes; n++)
	    obs->telescope[n].next = &obs->telescope[(n+1)%obs->ntelescopes];

	return (OK);
}
