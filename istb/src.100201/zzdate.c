#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "config.h"
#include "header.h"

/* FILE	*mp = stdout;
 */
FILE	*mp;

#define	ERR	0
#define	OK	1

int	messages = 0;
int	ngotten;

main (argc, argv)
int	argc;
char	*argv[];
{
	struct	fitsheader header;
	char	fname[SZ_PATHNAME], caldate[SZ_PATHNAME];
	char	date_obs[SZ_PATHNAME], date[SZ_PATHNAME], ut[SZ_PATHNAME];
	int     tz=TZ_DEFAULT, pivot=PIVOT;
	double	utx;
	FILE	*fp;

	if (argc <= 1)
	    exit (-1);

	strncpy (fname, argv[1], SZ_PATHNAME);

	if ((fp = fopen (fname, "r")) == NULL) {
	    printf ("error opening file %s\n", fname);
	    exit (-1);
	}

	if (read_header (fp, &header)) {
/*	    print_header (stdout, header.buf, header.ncards);
 *	    short_description (stdout, header);
 *	    describe_header (stdout, header, 2);
 */

	    strncpy (date_obs, header.date_obs, SZ_PATHNAME);
	    strncpy (date, header.date, SZ_PATHNAME);
	    strncpy (ut, header.ut, SZ_PATHNAME);

/*	    printf ("DATE-OBS = %s\n", date_obs);
 *	    printf ("DATE     = %s\n", date);
 *	    printf ("UT       = %s", ut);
 */
	    if (sex2x (ut, &utx) == OK)
		printf (" = %g\n", utx);
	    else
		printf ("\n");

	    if (getcaldate (date_obs, date, ut, tz, pivot, caldate))
		printf ("DTS_DATE = %s\n", caldate);

	    free (header.buf);

	} else {
	    printf ("error opening FITS file %s\n", fname);
	}

	fclose (fp);
	exit (0);
}
