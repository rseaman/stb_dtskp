/* DTSSTATUS - implemented as a filter specific to LPRng.
 *
 * Call this from a pipeline:
 *     lpc status <queue> | grep <queue> | dtsstatus
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "config.h"
#include "status.h"

main (argc, argv)
int	argc;
char	*argv[];
{
	struct	queue_status	stb;
	char	name[SZ_PATHNAME];
	char	pstring[SZ_PATHNAME];
	char	qstring[SZ_PATHNAME];
	int	nqueue, pstat, qstat;

	/* query the lpd queue status
	 */
	scanf ("%s %s %s %d", name, pstring, qstring, &nqueue);

	if (strcmp (pstring, "enabled") == 0)
	    pstat = 1;
	else if (strcmp (pstring, "disabled") == 0)
	    pstat = 0;
	else
	    pstat = -1;

	if (strcmp (qstring, "enabled") == 0)
	    qstat = 1;
	else if (strcmp (qstring, "disabled") == 0)
	    qstat = 0;
	else
	    qstat = -1;

	/* query the dts status
	 */
        if (! read_status (STATUS, &stb)) {
	    printf ("status file %s not available\n", STATUS);
	    exit (-1);
	}

	/* format the output
	 */
	printf ("%-12s             ", name);

	if (stb.status == STAGING)
	    printf ("STAGING");
	else if (stb.status == STOPPED)
	    printf ("STOPPED");
	else
	    printf ("ERROR  ");

	printf ("        %s (report)\n", time_string (now()));

	if (qstat > 0)
	    printf ("queue enabled ");
	else if (qstat == 0)
	    printf ("queue disabled");
	else
	    printf ("queue unknown ");

	printf ("   %7d pending", nqueue);

	printf ("        %s (latest)\n", time_string (stb.time));

	if (pstat > 0)
	    printf ("cache enabled ");
	else if (pstat == 0)
	    printf ("cache disabled");
	else
	    printf ("cache unknown ");

	printf ("   %7d done\n", stb.recno);

	exit (0);
}
