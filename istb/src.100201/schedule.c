#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "config.h"
#include "schedule.h"

#define	ERR		0
#define	OK		1

int get_schedule (telescope, date, sinfo)
char	*telescope;
char	*date;
struct	daily_schedule *sinfo;
{
	char	schedfile[SZ_PATHNAME], date1[SZ_PATHNAME];
	char	key[SZ_PATHNAME], value[SZ_PATHNAME], var[SZ_PATHNAME];
	int	year, month, day, n, na, ivalue, firsttime=1;
	int	got1=0, got2=0, got3=0, got4=0, got5=0, got6=0, got7=0;
	FILE	*fp;

	strcpy (date1, date);
	if (date1[8] == 'e') { date1[8] = (char) NULL; }

	if (! dtm_decode_short (date1, &year, &month, &day))
	    return (ERR);

	if (year < MINYEAR || year > MAXYEAR)
	    return (ERR);

	if (month < 1 || month > 12)
	    return (ERR);

	if (day < 1 || day > month_length (year, month))
	    return (ERR);

	sprintf (schedfile, "%s/%4d/%s.%4d-%02d",
	    SCHEDULE, year, telescope, year, month);

	if ((fp = fopen (schedfile, "r")) == NULL)
	    return (ERR);

	strcpy (sinfo->institution, "unknown");
	strcpy (sinfo->proposal, "unknown");
	strcpy (sinfo->investigator, "unknown");
	strcpy (sinfo->affiliation, "unknown");
	strcpy (sinfo->title, "unknown");
	strcpy (sinfo->copyright, "unknown");
	sinfo->proprietary = -1;

	while (! feof (fp)) {
	    if (got1 && got2 && got3 && got4 && got5 && got6 && got7) { break; }

	    if (fscanf (fp, " %s = %[^\n] ", key, value) != 2)
		continue;

	    if (isdigit (value[0]))
		ivalue = atoi (value);
	    else
		ivalue = -1;

	    sprintf (var, "institution[%s]", date1);
	    if (strcmp (key, var) == 0) {
		strncpy (sinfo->institution, value, SZ_PATHNAME);
		got1++; continue;
	    }

	    sprintf (var, "proposal[%s]", date1);
	    if (strcmp (key, var) == 0) {
		strncpy (sinfo->proposal, value, SZ_PATHNAME);
		got2++; continue;
	    }

	    sprintf (var, "investigator[%s]", date1);
	    if (strcmp (key, var) == 0) {
		strncpy (sinfo->investigator, value, SZ_PATHNAME);
		got3++; continue;
	    }

	    sprintf (var, "affiliation[%s]", date1);
	    if (strcmp (key, var) == 0) {
		strncpy (sinfo->affiliation, value, SZ_PATHNAME);
		got4++; continue;
	    }

	    sprintf (var, "title[%s]", date1);
	    if (strcmp (key, var) == 0) {
		strncpy (sinfo->title, value, SZ_PATHNAME);
		got5++; continue;
	    }

	    sprintf (var, "copyright[%s]", date1);
	    if (strcmp (key, var) == 0) {
		strncpy (sinfo->copyright, value, SZ_PATHNAME);
		got6++; continue;
	    }

	    sprintf (var, "proprietary[%s]", date1);
	    if (strcmp (key, var) == 0) {
		sinfo->proprietary = ivalue;
		got7++; continue;
	    }
	}

	if (strcmp (sinfo->institution, "unknown") == 0)
	    sinfo->institution[0] = (char) NULL;
	if (strcmp (sinfo->proposal, "unknown") == 0)
	    sinfo->proposal[0] = (char) NULL;
	if (strcmp (sinfo->investigator, "unknown") == 0)
	    sinfo->investigator[0] = (char) NULL;
	if (strcmp (sinfo->affiliation, "unknown") == 0)
	    sinfo->affiliation[0] = (char) NULL;
	if (strcmp (sinfo->title, "unknown") == 0)
	    sinfo->title[0] = (char) NULL;
	if (strcmp (sinfo->copyright, "unknown") == 0)
	    sinfo->copyright[0] = (char) NULL;

	fclose (fp);
	return (OK);
}
