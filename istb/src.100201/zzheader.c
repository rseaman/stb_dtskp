#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "header.h"

#define	ERR		0
#define	OK		1

int	ngotten;

int	messages = 2;
FILE	*mp = NULL;

main (argc, argv)
int     argc;
char    *argv[];
{
	struct	fitsheader	inheader, outheader;
	FILE *fp;

        mp = stdout;

	fp = fopen (argv[1], "r");

	if (! read_header (fp, &inheader)) {
	    printf ("error reading input header\n");
	    exit (-1);
	}

	if (! copy_header (inheader, &outheader, 13, 1)) {
	    printf ("error copying input to output header\n");
	    exit (-1);
	}

	describe_header (mp, outheader, 4);

	fclose (mp);
}
