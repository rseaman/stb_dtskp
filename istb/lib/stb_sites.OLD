# Master list of sites, telescopes and instruments
# supported by the NOAO Science Archive
# 1 February 2007, RLS

nsites               = 5
ntelescopes          = 14
ninstruments         = 28
nhosts               = 61

nalternates          = 2

# A locale is a distinct instance of iSTB server and daemon.  A single
# server could host more than one daemon, but each must have a different
# locale.  Each locale manages a separate run of serial numbers which
# must be disambiguated by prefixing a string unique to the locale.
# Daemons may write into the same directory heirarchies and data
# from any source computer may be fed to any daemon.
# A site is a unique data source location.  A locale is a unique sink.
istb.locale          = kp

# A site is a distinct physical location with a common timezone.
# Raw data are tagged by DT keywords and sorted into directories.
# Pipeline data has telescope of "none" and are sorted into directories
# by header keywords named by dir[1|2|3]_key.  Daemon will halt if
# keywords are absent or unreadable.
site[0].name         = none
site[0].timezone     = -7
site[0].root_dir     = /noaocache/pipeline
site[0].dir1_key     = PLQUEUE
site[0].dir2_key     = PLQNAME
site[0].dir3_key     = PLPROCID

site[1].name         = kp
site[1].timezone     = -7
site[1].root_dir     = /noaocache/pipeline
site[1].dir1_key     = PLQUEUE
site[1].dir2_key     = PLQNAME
site[1].dir3_key     = PLPROCID

site[2].name         = ct
site[2].timezone     = -4
site[2].root_dir     = /noaocache/pipeline
site[2].dir1_key     = PLQUEUE
site[2].dir2_key     = PLQNAME
site[2].dir3_key     = PLPROCID

site[3].name         = cp
site[3].timezone     = -4
site[3].root_dir     = /noaocache/pipeline
site[3].dir1_key     = PLQUEUE
site[3].dir2_key     = PLQNAME
site[3].dir3_key     = PLPROCID

site[4].name         = tu
site[4].timezone     = -7
site[4].root_dir     = /noaocache/pipeline
site[4].dir1_key     = PLQUEUE
site[4].dir2_key     = PLQNAME
site[4].dir3_key     = PLPROCID

# A telescope would typically include all optical
# configurations using a particular primary mirror.
# Telescope = "none" is used with pipeline and other DMS queues
telescope[0].name    = none

telescope[1].name    = kp4m
telescope[2].name    = kp21m
telescope[3].name    = kpcf
telescope[4].name    = kp09m

telescope[5].name    = mcmath

telescope[6].name    = wiyn

telescope[7].name    = ct4m
telescope[8].name    = ct15m
telescope[9].name    = ct13m
telescope[10].name   = ct1m
telescope[11].name   = ct09m

telescope[12].name   = soar

telescope[13].name   = dms

# An instrument for cache purposes is distinguished by
# a specific combination of data acquisition software
# and host computer.  Classification by header
# semantics is a function of archive ingest.

instrument[0].name   = none

# defaults for instruments that can't be named explicitly
instrument[1].name   = ccd_imager
instrument[2].name   = ccd_spec
instrument[3].name   = ir_imager
instrument[4].name   = ir_spec

instrument[5].name   = mosaic_1
instrument[6].name   = mars
instrument[7].name   = rcspec
instrument[8].name   = echelle
instrument[9].name   = sqiid
instrument[10].name  = flamingos

instrument[11].name  = gcam

instrument[12].name  = minimosaic
instrument[13].name  = wttm
instrument[14].name  = hydra
instrument[15].name  = densepack
instrument[16].name  = sparsepack
instrument[17].name  = optic

instrument[18].name  = mosaic_2
instrument[19].name  = ispi

instrument[20].name  = cpapir

instrument[21].name  = andicam

instrument[22].name  = soi
instrument[23].name  = osiris
instrument[24].name  = goodman

instrument[25].name  = pipeline

instrument[26].name  = y4kcam

instrument[27].name  = newfirm


# List of data acquisition hosts connected to iSTB.
# This list must be updated when new hosts become
# operational.

host[0].name[0]      = none
host[0].name[1]      = none
host[0].site         = 0
host[0].telescope    = 0
host[0].instrument   = 0

# DHS
host[1].name[0]      = tan
host[1].name[1]      = tan.kpno.noao.edu
host[1].site         = 1
host[1].telescope    = 1
host[1].instrument   = 5

# DHS (retired)
host[2].name[0]      = pecan
host[2].name[1]      = pecan.kpno.noao.edu
host[2].site         = 1
host[2].telescope    = 1
host[2].instrument   = 5

# DHS (spare)
host[3].name[0]      = nutmeg
host[3].name[1]      = nutmeg.kpno.noao.edu
host[3].site         = 1
host[3].telescope    = 1
host[3].instrument   = 5

# ICE, Wildfire
host[4].name[0]      = khaki
host[4].name[1]      = khaki.kpno.noao.edu
host[4].site         = 1
host[4].telescope    = 1
host[4].instrument   = 2

# ICE, Wildfire
# host[4].name[0]      = khaki
# host[4].name[1]      = khaki.kpno.noao.edu
# host[4].site         = 1
# host[4].telescope    = 1
# host[4].instrument   = 0

# Flamingos
host[5].name[0]      = flmn-4m-1a
host[5].name[1]      = flmn-4m-1a.kpno.noao.edu
host[5].site         = 1
host[5].telescope    = 1
host[5].instrument   = 10

# Flamingos (spare)
host[6].name[0]      = flmn-4m-1b
host[6].name[1]      = flmn-4m-1b.kpno.noao.edu
host[6].site         = 1
host[6].telescope    = 1
host[6].instrument   = 10

# ICE
host[7].name[0]      = lapis
host[7].name[1]      = lapis.kpno.noao.edu
host[7].site         = 1
host[7].telescope    = 2
host[7].instrument   = 2

# Wildfire
host[8].name[0]      = royal
host[8].name[1]      = royal.kpno.noao.edu
host[8].site         = 1
host[8].telescope    = 2
host[8].instrument   = 3

# Flamingos
host[9].name[0]      = flmn-2m-1a
host[9].name[1]      = flmn-2m-1a.kpno.noao.edu
host[9].site         = 1
host[9].telescope    = 2
host[9].instrument   = 10

# Flamingos (spare)
host[10].name[0]      = flmn-2m-1b
host[10].name[1]      = flmn-2m-1b.kpno.noao.edu
host[10].site         = 1
host[10].telescope    = 2
host[10].instrument   = 10

#ICE
host[11].name[0]      = indigo
host[11].name[1]      = indigo.kpno.noao.edu
host[11].site         = 1
host[11].telescope    = 3
host[11].instrument   = 2

# DHS
host[12].name[0]      = emerald
host[12].name[1]      = emerald.kpno.noao.edu
host[12].site         = 1
host[12].telescope    = 4
host[12].instrument   = 5

# DHS (spare)
host[13].name[0]      = driftwood
host[13].name[1]      = driftwood.kpno.noao.edu
host[13].site         = 1
host[13].telescope    = 4
host[13].instrument   = 5

# ICE
host[14].name[0]      = taupe
host[14].name[1]      = taupe.kpno.noao.edu
host[14].site         = 1
host[14].telescope    = 4
host[14].instrument   = 1

# Optic
host[15].name[0]      = mrsbill
host[15].name[1]      = mrsbill.kpno.noao.edu
host[15].site         = 1
host[15].telescope    = 4
host[15].instrument   = 17

# ICE (not connected to DTS)
host[16].name[0]      = pacifico
host[16].name[1]      = pacifico.kpno.noao.edu
host[16].site         = 1
host[16].telescope    = 5
host[16].instrument   = 2

# DHS (dual instrument)
host[17].name[0]      = sand
host[17].name[1]      = sand.kpno.noao.edu
host[17].site         = 1
host[17].telescope    = 6
host[17].instrument   = 1

# Arcon (retired)
host[18].name[0]      = pearl
host[18].name[1]      = pearl.kpno.noao.edu
host[18].site         = 1
host[18].telescope    = 6
host[18].instrument   = 1

# DHS (spare?)
host[19].name[0]      = navajo
host[19].name[1]      = navajo.kpno.noao.edu
host[19].site         = 1
host[19].telescope    = 6
host[19].instrument   = 13

# Arcon (retired?)
host[20].name[0]      = vanilla
host[20].name[1]      = vanilla.kpno.noao.edu
host[20].site         = 1
host[20].telescope    = 6
host[20].instrument   = 14

# ICE (temporary?)
host[21].name[0]      = white
host[21].name[1]      = white.kpno.noao.edu
host[21].site         = 1
host[21].telescope    = 6
host[21].instrument   = 14

# Optic
host[22].name[0]      = mrbill
host[22].name[1]      = mrbill.kpno.noao.edu
host[22].site         = 1
host[22].telescope    = 6
host[22].instrument   = 17

# Arcon?
host[23].name[0]      = ctioa1
host[23].name[1]      = ctioa1.ctio.noao.edu
host[23].site         = 2
host[23].telescope    = 7
host[23].instrument   = 2

# DHS
host[24].name[0]      = ctioa9
host[24].name[1]      = ctioa9.ctio.noao.edu
host[24].site         = 2
host[24].telescope    = 7
host[24].instrument   = 19

# ISPI?
host[25].name[0]      = ctioa8
host[25].name[1]      = ctioa8.ctio.noao.edu
host[25].site         = 2
host[25].telescope    = 7
host[25].instrument   = 18

# Arcon?
host[26].name[0]      = ctioa2
host[26].name[1]      = ctioa2.ctio.noao.edu
host[26].site         = 2
host[26].telescope    = 8
host[26].instrument   = 2

# Andicam
host[27].name[0]      = andicam
host[27].name[1]      = andicam.ctio.noao.edu
host[27].site         = 2
host[27].telescope    = 9
host[27].instrument   = 21

# Arcon?
host[28].name[0]      = ctioa4
host[28].name[1]      = ctioa4.ctio.noao.edu
host[28].site         = 2
host[28].telescope    = 11
host[28].instrument   = 1

# iSTB
host[29].name[0]      = dtskp
host[29].name[1]      = dtskp.kpno.noao.edu
host[29].site         = 1
host[29].telescope    = 0
host[29].instrument   = 0

# NEWFIRM
# host[29].name[0]      = dtskp
# host[29].name[1]      = dtskp.kpno.noao.edu
# host[29].site         = 1
# host[29].telescope    = 1
# host[29].instrument   = 27

# iSTB
host[30].name[0]      = dtsct
host[30].name[1]      = dtsct.ctio.noao.edu
host[30].site         = 2
host[30].telescope    = 0
host[30].instrument   = 0

# STB
host[31].name[0]      = claret
host[31].name[1]      = claret.kpno.noao.edu
host[31].site         = 1
host[31].telescope    = 0
host[31].instrument   = 0

# STB
host[32].name[0]      = sangria
host[32].name[1]      = sangria.kpno.noao.edu
host[32].site         = 1
host[32].telescope    = 0
host[32].instrument   = 0

# STB
host[33].name[0]      = ctiot7
host[33].name[1]      = ctiot7.ctio.noao.edu
host[33].site         = 2
host[33].telescope    = 0
host[33].instrument   = 0

# STB
host[34].name[0]      = ctiot5
host[34].name[1]      = ctiot5.ctio.noao.edu
host[34].site         = 2
host[34].telescope    = 0
host[34].instrument   = 0

# Mosaic II?
host[35].name[0]      = ctiot8
host[35].name[1]      = ctiot8.ctio.noao.edu
host[35].site         = 2
host[35].telescope    = 7
host[35].instrument   = 18

# SOAR Optical Imager
host[36].name[0]      = soaric1
host[36].name[1]      = soaric1.ctio.noao.edu
host[36].site         = 3
host[36].telescope    = 12
host[36].instrument   = 22

# SOAR OSIRIS
host[37].name[0]      = soaric2
host[37].name[1]      = soaric2.ctio.noao.edu
host[37].site         = 3
host[37].telescope    = 12
host[37].instrument   = 23

# SOAR Goodman Spectrograph
host[38].name[0]      = soaric5
host[38].name[1]      = soaric5.ctio.noao.edu
host[38].site         = 3
host[38].telescope    = 12
host[38].instrument   = 24

# SOAR (spare)
host[39].name[0]      = soaric9
host[39].name[1]      = soaric9.ctio.noao.edu
host[39].site         = 3
host[39].telescope    = 12
host[39].instrument   = 22

# SOAR (spare)
host[40].name[0]      = ctioyg
host[40].name[1]      = ctioyg.ctio.noao.edu
host[40].site         = 3
host[40].telescope    = 12
host[40].instrument   = 22

# PIPELINE
host[41].name[0]      = pipen01
host[41].name[1]      = pipen01.tuc.noao.edu
host[41].site         = 4
host[41].telescope    = 13
host[41].instrument   = 25

# PIPELINE
host[42].name[0]      = pipen02
host[42].name[1]      = pipen02.tuc.noao.edu
host[42].site         = 4
host[42].telescope    = 13
host[42].instrument   = 25

# PIPELINE
host[43].name[0]      = pipen03
host[43].name[1]      = pipen03.tuc.noao.edu
host[43].site         = 4
host[43].telescope    = 13
host[43].instrument   = 25

# PIPELINE
host[44].name[0]      = pipen04
host[44].name[1]      = pipen04.tuc.noao.edu
host[44].site         = 4
host[44].telescope    = 13
host[44].instrument   = 25

# PIPELINE
host[45].name[0]      = pipen05
host[45].name[1]      = pipen05.tuc.noao.edu
host[45].site         = 4
host[45].telescope    = 13
host[45].instrument   = 25

# PIPELINE
host[46].name[0]      = pipen06
host[46].name[1]      = pipen06.tuc.noao.edu
host[46].site         = 4
host[46].telescope    = 13
host[46].instrument   = 25

# PIPELINE
host[47].name[0]      = pipen07
host[47].name[1]      = pipen07.tuc.noao.edu
host[47].site         = 4
host[47].telescope    = 13
host[47].instrument   = 25

# PIPELINE
host[48].name[0]      = pipen08
host[48].name[1]      = pipen08.tuc.noao.edu
host[48].site         = 4
host[48].telescope    = 13
host[48].instrument   = 25

# PIPELINE
host[49].name[0]      = pipedmn
host[49].name[1]      = pipedmn.tuc.noao.edu
host[49].site         = 4
host[49].telescope    = 13
host[49].instrument   = 25

# PIPELINE
host[50].name[0]      = pipedm
host[50].name[1]      = pipedm.tuc.noao.edu
host[50].site         = 4
host[50].telescope    = 13
host[50].instrument   = 25

# PIPELINE
host[51].name[0]      = pipen2
host[51].name[1]      = pipen2.tuc.noao.edu
host[51].site         = 4
host[51].telescope    = 13
host[51].instrument   = 25

# PIPELINE
host[52].name[0]      = pipedevn
host[52].name[1]      = pipedevn.tuc.noao.edu
host[52].site         = 4
host[52].telescope    = 13
host[52].instrument   = 25

# PIPELINE
host[53].name[0]      = archivedm
host[53].name[1]      = archivedm.tuc.noao.edu
host[53].site         = 4
host[53].telescope    = 0
host[53].instrument   = 25

# Y4KCAM
host[54].name[0]      = yale1m
host[54].name[1]      = yale1m.ctio.noao.edu
host[54].site         = 2
host[54].telescope    = 10
host[54].instrument   = 26

# CTIO SPARE
host[55].name[0]      = ctioa0
host[55].name[1]      = ctioa0.ctio.noao.edu
host[55].site         = 2
host[55].telescope    = 7
host[55].instrument   = 2

# CTIO SPARE
host[56].name[0]      = dtsct-hs
host[56].name[1]      = dtsct-hs.ctio.noao.edu
host[56].site         = 2
host[56].telescope    = 0
host[56].instrument   = 0

# KPNO SPARE
host[57].name[0]      = dtskp-spare
host[57].name[1]      = dtskp-spare.kpno.noao.edu
host[57].site         = 1
host[57].telescope    = 0
host[57].instrument   = 0

# NEWFIRM
host[58].name[0]      = nfdca-KP
host[58].name[1]      = nfdca-KP.kpno.noao.edu
host[58].site         = 1
host[58].telescope    = 1
host[58].instrument   = 27

# NEWFIRM
host[59].name[0]      = nfdca-FR
host[59].name[1]      = nfdca-FR.tuc.noao.edu
host[59].site         = 4
host[59].telescope    = 0
host[59].instrument   = 27

# iSTB
host[60].name[0]      = dtscp
host[60].name[1]      = dtscp.ctio.noao.edu
host[60].site         = 3
host[60].telescope    = 0
host[60].instrument   = 0
